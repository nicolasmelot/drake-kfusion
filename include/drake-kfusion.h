#include <kernels.h>
#include <semaphore.h>

#ifndef DRAKE_KFUSION
#define DRAKE_KFUSION 1

typedef struct
{
	int integration_rate, tracking_rate, rendering_rate;
	const unsigned short *in;
	float *gaussian;
	float e_d;
	size_t r;
	uint2 inSize, compSize;
	std::vector<int> pyramid;
	float4 k;
	Matrix4 pose;
	float4 view;
	float dist_threshold, normal_threshold;
	float icp_threshold;
	float mu;
	float maxweight;
	float track_threshold;
	float largestep;
	Volume *volume;
	float nearPlane;
	float farPlane;
	float step;
	sem_t frame_queue, frame_consume, frame_output;
	int no_more_frame;
	float3 light;
	float3 ambient;
	Matrix4 **viewPose;
	void *render_depth_buffer, *render_track_buffer;
	uchar4 *render_volume_buffer;
	int render_depth, render_track, render_volume;
	sem_t render_depth_done, render_track_done, render_volume_done;
} drake_kfusion_t;

#endif
