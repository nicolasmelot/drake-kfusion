/*
 Copyright 2015 Nicolas Melot

 This file is part of Drake-merge.

 Drake-merge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Drake-merge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Drake-merge. If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <math.h>
#include <rapl.h>

#include <drake.h>
#include <drake/link.h>
#include <string.h>

#include <drake-kfusion.h> 
#include "kernels.h"

drake_declare_input(input, float);
drake_declare_output(output, float);
drake_declare_output(render, float);
//static drake_time_t start, stop, duration;

static drake_kfusion_t *args;
//static link_tp input_link, output_link, render_link;

int
drake_init(void* arg)
{
/*
	input_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "input")).value;
	output_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "output")).value;
	render_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "render")).value;
*/
/*
	start = drake_platform_time_alloc();
	stop = drake_platform_time_alloc();
	duration = drake_platform_time_alloc();
*/
	args = (drake_kfusion_t*)arg;
	return 1;
}

int
drake_start()
{
	return 1;
}

#define PARALLEL_BILATERAL 1
#if PARALLEL_BILATERAL
struct bilateral_filter_args
{
	float *output, *input, *gaussian;
	uint2 *compSize;
	float e_d;
	size_t r;
};

static int
parallel_bilateral_filter(void *aux)
{
	struct bilateral_filter_args *args = (struct bilateral_filter_args*)aux;
	if(drake_task_core_id() == 0)
	{
	//bilateralFilterKernel(args->output, args->input, *args->compSize, args->gaussian, args->e_d, args->r, drake_task_core_id(), drake_task_width());
	bilateralFilterKernel(args->output, args->input, *args->compSize, args->gaussian, args->e_d, args->r, 0, 1);
	}

	return 1;
}
#endif

int
drake_run()
{
	// Fetch buffer addresses
	size_t input_size = 0;
	//float* input = (float*)pelib_cfifo_peekaddr(int)(input_link->buffer, 0, &input_size, NULL);
	float *input = drake_input_buffer(input)(0, &input_size, NULL);

	size_t output_size;
	//float *output = (float*)pelib_cfifo_writeaddr(int)(output_link->buffer, &output_size, NULL);
	float *output = drake_output_buffer(output)(&output_size, NULL);

	size_t render_size;
	//float *render = (float*)pelib_cfifo_writeaddr(int)(render_link->buffer, &output_size, NULL);
	float *render = drake_output_buffer(render)(&render_size, NULL);

	size_t inSize = args->compSize.x * args->compSize.y;
	size_t outSize = args->compSize.x * args->compSize.y;

#if DEBUG_INCONSISTENCIES
	//if(inSize > pelib_cfifo_capacity(int)(input_link->buffer) || outSize > pelib_cfifo_capacity(int)(output_link->buffer))
	if(inSize > drake_input_capacity(input) || outSize > drake_output_capacity(output))
	{
		//printf("[%s:%s:%d] %s buffer cannot fit input or output frame (%zu > %zu)\n", __FILE__, __FUNCTION__, __LINE__, inSize > pelib_cfifo_capacity(int)(input_link->buffer) ? "Input" : "Output", inSize > pelib_cfifo_capacity(int)(input_link->buffer) ? inSize : outSize, inSize > pelib_cfifo_capacity(int)(input_link->buffer) ? pelib_cfifo_capacity(int)(input_link->buffer) : pelib_cfifo_capacity(int)(output_link->buffer));
		printf("[%s:%s:%d] %s buffer cannot fit input or output frame (%zu > %zu)\n", __FILE__, __FUNCTION__, __LINE__, inSize > drake_input_capacity(input) ? "Input" : "Output", inSize > drake_input_capacity(input) ? inSize : outSize, inSize > drake_input_capacity(input) ? drake_input_capacity(input) : drake_output_capacity(output));
		abort();
	}
#endif

	if(input_size > inSize)
	{
		//pelib_cfifo_discard(int)(input_link->buffer, input_size);
		drake_input_discard(input)(input_size - inSize);
		input = drake_input_buffer(input)(0, &input_size, NULL);
	}


	//render = (float*)pelib_cfifo_writeaddr(int)(render_link->buffer, &render_size, &render_wrap);

	if(
		   input_size >= inSize
		&& output_size >= outSize
	)
	{
	if(output_size > outSize)
	{
		//pelib_cfifo_fill(int)(output_link->buffer, output_size);
		drake_output_commit(output)(output_size - outSize);
		output = drake_output_buffer(output)(&output_size, NULL);
	}
	if(render_size > outSize)
	{
		//pelib_cfifo_fill(int)(render_link->buffer, render_size);
		drake_output_commit(render)(render_size - outSize);
		render = drake_output_buffer(render)(&render_size, NULL);
	}
//	drake_platform_time_get(start);
//	uint64_t tsc_start, tsc_stop;
/*
	float floatDepthTotal = 0;
	for(unsigned int i = 0; i < args->compSize.x * args->compSize.y; i++)
	{
		floatDepthTotal += input[i];
	}
	float gaussianTotal = 0;
	int r = args->r;
	for(int i = -r; i <= r; i++)
	{
		gaussianTotal += args->gaussian[i + args->r];
	}
*/
/*
	debug(floatDepthTotal);
	debug(args->compSize.x);
	debug(args->compSize.y);
	debug(gaussianTotal);
	debug(args->e_d);
	debug(args->r);
*/
	//read_tsc(&tsc_start);
#if PARALLEL_BILATERAL
		struct bilateral_filter_args pargs;
		pargs.output = output;
		pargs.input = input;
		pargs.compSize = &args->compSize;
		pargs.gaussian = args->gaussian;
		pargs.e_d = args->e_d;
		pargs.r = args->r;
		drake_task_pool_run(parallel_bilateral_filter, &pargs);
#else
		bilateralFilterKernel(output, input, args->compSize, args->gaussian, args->e_d, args->r, 0, 1);
#endif
	//read_tsc(&tsc_stop);
	//printf("bilateral: %lu\n", tsc_stop - tsc_start);
	//drake_platform_time_get(stop);

		//pelib_cfifo_discard(int)(input_link->buffer, inSize);
		drake_input_discard(input)(inSize);
		memcpy(render, output, sizeof(float) * outSize);
		//pelib_cfifo_fill(int)(output_link->buffer, outSize);
		//pelib_cfifo_fill(int)(render_link->buffer, outSize);
		drake_output_commit(output)(outSize);
		drake_output_commit(render)(outSize);
	//drake_platform_time_subtract(duration, stop, start);
	//printf("bilateral: ");
	//drake_platform_time_printf(stdout, duration);
	//printf("\n");
	}
	else
	{
		// Either input channel is empty, either it is full of garbage because
		// of buffer wrapping performed by last task. In the latter case, we
		// should get rid of it. In the former case, we just discard 0 element.
	}

		// Wrap buffer around if remainder memory is too small
		//outSize = args->compSize.x * args->compSize.y;
		//float *output_wrap;
		//output = (float*)pelib_cfifo_writeaddr(int)(output_link->buffer, &output_size, &output_wrap);
	int res = drake_task_autoexit();
	//debug(res);
	return res;
}

int
drake_kill()
{
	//debug("bilateral_filter");
	//drake_platform_time_get(killed[task->id - 1]);

	// Do nothing
	return 0;
}

int
drake_destroy()
{
	// Do nothing
	return 1;
}


