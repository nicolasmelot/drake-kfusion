/*
 Copyright 2015 Nicolas Melot

 This file is part of Drake-merge.

 Drake-merge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Drake-merge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Drake-merge. If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <sys/mman.h>
#include <float.h>

#include <drake.h>
#include <drake/link.h>
#include <string.h>

#include <drake-kfusion.h> 
#include "kernels.h"
#include "util.h"

drake_declare_input(input, float);
drake_declare_output(normal, float);
drake_declare_output(track, float);

static drake_kfusion_t *args;
static size_t i;
static int frame = 0;
//static drake_time_t start, stop, duration;

//static link_tp input_link, normal_link, track_link;

int
drake_init(void* arg)
{
/*
	input_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "input")).value;
	normal_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "normal")).value;
	track_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "track")).value;
*/

	args = (drake_kfusion_t*)arg;
/*
	start = drake_platform_time_alloc();
	stop = drake_platform_time_alloc();
	duration = drake_platform_time_alloc();
*/
	i = 0;
	return 1;
}

int
drake_start()
{
	return 1;
}

#define PARALLEL_D2V 1
#if PARALLEL_D2V
struct d2v_args
{
	drake_kfusion_t* args;
	Matrix4 *invK4;
	uint2 size;
	float *track, *input;
};

static int
parallel_d2v(void *aux)
{
	struct d2v_args *args = (struct d2v_args*)aux;
	depth2vertexKernel((float3*)args->track, args->input, args->size, *args->invK4, drake_task_core_id(), drake_task_width());

	return 1;
}
#endif

int
drake_run()
{
	// Fetch links
	// Fetch buffer addresses
	size_t input_size = 0;
	//float* input = (float*)pelib_cfifo_peekaddr(int)(input_link->buffer, 0, &input_size, NULL);
	float *input = drake_input_buffer(input)(0, &input_size, NULL);

	size_t normal_size;
	//float *normal = (float*)pelib_cfifo_writeaddr(int)(normal_link->buffer, &normal_size, NULL);
	float *normal = drake_output_buffer(normal)(&normal_size, NULL);
	size_t track_size;
	//float *track = (float*)pelib_cfifo_writeaddr(int)(track_link->buffer, &track_size, NULL);
	float *track = drake_output_buffer(track)(&track_size, NULL);

	size_t inSize = args->compSize.x * args->compSize.y / pow(4, args->pyramid.size() - i - 1);
	size_t outSize = inSize * 3;

	// Compile-time check: can we really stream float3?
	static_assert(std::is_trivially_copyable<float3>::value, "float3 is not trivially copyable");

#if DEBUG_INCONSISTENCIES
	//if(inSize > pelib_cfifo_capacity(int)(input_link->buffer) || outSize > pelib_cfifo_capacity(int)(normal_link->buffer) || outSize > pelib_cfifo_capacity(int)(track_link->buffer))
	if(inSize > drake_input_capacity(input) || outSize > drake_output_capacity(normal) || outSize > drake_output_capacity(track))
	{
		printf("[%s:%s:%d] %s buffer cannot fit input or output frame (%zu > %zu)\n", __FILE__, __FUNCTION__, __LINE__, inSize > drake_input_capacity(input) ? "Input" : "Output", inSize > drake_input_capacity(input) ? inSize : outSize, inSize > drake_input_capacity(input) ? drake_input_capacity(input) : drake_output_capacity(normal));
		abort();
	}
#endif

	size_t totalSize = skip(args->compSize.x * args->compSize.y, args->pyramid.size() - i, args->pyramid.size() - 0);

	if(input_size > totalSize)
	{
		//pelib_cfifo_discard(int)(input_link->buffer, input_size);
		drake_input_discard(input)(input_size - totalSize);
		input = drake_input_buffer(input)(0, &input_size, NULL);
	}

	//track = (float*)pelib_cfifo_writeaddr(int)(track_link->buffer, &track_size, &track_wrap);

	//normal = (float*)pelib_cfifo_writeaddr(int)(normal_link->buffer, &normal_size, &normal_wrap);

	size_t skip_size = skip(args->compSize.x * args->compSize.y, args->pyramid.size() - i, args->pyramid.size() - i);
#define DEBUG_GUARD 0
#if DEBUG_GUARD
debug("##### Depth to Vertex #####");
debug(inSize);
debug(skip_size);
debug(outSize);
debug(input_size);
debug(normal_size);
debug(track_size);
debug("########## End ############");
#endif
	if(
		   input_size >= skip_size
		&& normal_size >= outSize
		&& track_size >= outSize
	)
	{
		//debug(drake_task_instance());
		float4 cam4 = args->k / (float)(1 << (args->pyramid.size() - i - 1));
		Matrix4 invK4 = getInverseCameraMatrix(cam4);
		uint2 size = make_uint2(args->compSize.x / pow(2, args->pyramid.size() - i - 1), args->compSize.y / pow(2, args->pyramid.size() - i - 1));
	if(normal_size > outSize)
	{
		//pelib_cfifo_fill(int)(normal_link->buffer, normal_size);
		//debug(normal_size);
		//debug(outSize);
		drake_output_commit(normal)(normal_size - outSize);
		normal = drake_output_buffer(normal)(&normal_size, NULL);
	}
	if(track_size > outSize)
	{
		//pelib_cfifo_fill(int)(track_link->buffer, track_size);
		//debug(track_size);
		//debug(outSize);
		drake_output_commit(track)(track_size - outSize);
		track = drake_output_buffer(track)(&track_size, NULL);
	}
//	drake_platform_time_get(start);
		//debug("depth to vertex");
#if PARALLEL_D2V
#warning Parallel depth to vertex
		struct d2v_args pargs;
		pargs.track = track;
		pargs.input = input;
		pargs.size = size;
		pargs.invK4 = &invK4;

		drake_task_pool_run(parallel_d2v, &pargs);
#else
#warning Sequential depth to vertex
		depth2vertexKernel((float3*)track, input, size, invK4, 0, 1);
#endif
//	drake_platform_time_get(stop);
		memcpy(normal, track, outSize * sizeof(float));

		//pelib_cfifo_discard(int)(input_link->buffer, inSize);
		drake_input_discard(input)(inSize);
		//pelib_cfifo_fill(int)(normal_link->buffer, outSize);
		drake_output_commit(normal)(outSize);
		//pelib_cfifo_fill(int)(track_link->buffer, outSize);
		drake_output_commit(track)(outSize);

		i = (i + 1) % args->pyramid.size();
		if(i == 0)
		{
			frame++;
		}
//	drake_platform_time_subtract(duration, stop, start);
//	printf("depth to vertex: ");
//	drake_platform_time_printf(stdout, duration);
//	printf("\n");
	}

	inSize = args->compSize.x * args->compSize.y / pow(4, args->pyramid.size() - i - 1);
	outSize = inSize * 3;

	{
		// Either input channel is empty, either it is full of garbage because
		// of buffer wrapping performed by last task. In the latter case, we
		// should get rid of it. In the former case, we just discard 0 element.
	}

	// See if there is enough room for next round

	//return drake_task_depleted(task);
	int res = drake_task_autoexit();
	//debug(res);
	return res;
}

int
drake_kill()
{
	//debug("depth_to_vertex");
	//drake_platform_time_get(killed[task->id - 1]);

	// Do nothing
	return 0;
}

int
drake_destroy()
{
	// Do nothing
	return 1;
}

