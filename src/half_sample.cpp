/*
 Copyright 2015 Nicolas Melot

 This file is part of Drake-merge.

 Drake-merge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Drake-merge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Drake-merge. If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <math.h>

#include <drake.h>
#include <drake/link.h>
#include <string.h>

#include <drake-kfusion.h> 
#include "kernels.h"
#include "util.h"

drake_declare_input(input, float);
drake_declare_output(output, float);

static drake_kfusion_t *args;
//static drake_time_t start, stop, duration;
//static link_tp input_link, output_link;

int
drake_init(void* arg)
{
	/*
	input_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "input")).value;
	output_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "output")).value;
	*/
	args = (drake_kfusion_t*)arg;
/*
	start = drake_platform_time_alloc();
	stop = drake_platform_time_alloc();
	duration = drake_platform_time_alloc();
*/

	// Initialization is always successful
	return 1;
}

int
drake_start()
{
	return 1;
}

#define PARALLEL_HALF_SAMPLE 1
#if PARALLEL_HALF_SAMPLE
struct half_sample_args
{
	drake_kfusion_t *args;
	float *input, *output;
	uint2 length;
};

static int
parallel_half_sample(void *aux)
{
	struct half_sample_args *args = (struct half_sample_args*)aux;
	halfSampleRobustImageKernel(args->output, args->input, args->length, args->args->e_d * 3, 1, drake_task_core_id(), drake_task_width());
	
	return 1;
}
#endif

int
drake_run()
{
	// Fetch links

	// Fetch buffer addresses
	size_t input_size = 0;
	//float* input = (float*)pelib_cfifo_peekaddr(int)(input_link->buffer, 0, &input_size, NULL);
	float *input = drake_input_buffer(input)(0, &input_size, NULL);

	size_t output_size;
	//float *output = (float*)pelib_cfifo_writeaddr(int)(output_link->buffer, &output_size, NULL);
	float *output = drake_output_buffer(output)(&output_size, NULL);

	size_t inSize = args->compSize.x * args->compSize.y;
	size_t outSize = skip(inSize, args->pyramid.size(), args->pyramid.size());

#if DEBUG_INCONSISTENCIES
	//if(inSize > pelib_cfifo_capacity(int)(input_link->buffer) || outSize > pelib_cfifo_capacity(int)(output_link->buffer))
	if(inSize > drake_input_capacity(input) || outSize > drake_output_capacity(output))
	{
		printf("[%s:%s:%d] %s buffer cannot fit input or output frame (%zu > %zu)\n", __FILE__, __FUNCTION__, __LINE__, inSize > drake_input_capacity(input) ? "Input" : "Output", inSize > drake_input_capacity(input) ? inSize : outSize, inSize > drake_input_capacity(input) ? drake_input_capacity(input) : drake_output_capacity(output));
		abort();
	}
#endif

	if(input_size > inSize)
	{
		//pelib_cfifo_discard(int)(input_link->buffer, input_size);
		drake_input_discard(input)(input_size - inSize);
		input = drake_input_buffer(input)(0, &input_size, NULL);
	}


	if(
		   input_size >= inSize
		&& output_size >= outSize
	)
	{
		// Complicated: take a deep breath!
		// This task takes a full resolution depth map and outputs several tokens of gradually lower resolution tokens.
		// This is the input buffer of args->compSize.x * args->compSize.y = 256 (each hyphen is 4 pixels)
		// |----------------------------------------------------------------|
		// 0                                                               256
		// We output all frames from the least resolution one to the unmodified frame. Assuming 4 half-resolution scaling
		// steps, we will need a big output buffer as follows (size of all subsequent necessary buffer shown below):
		// |-|----|----------------|----------------------------------------------------------------|
		// <4><16-><-----64--------><-----------------------256------------------------------------->
		// Which results in the following offsets between each output token's buffers
		// 0-4---16---------------84--------------------------------------------------------------340
		// The total output buffer size we need is the space for 340 pixels. This number is obtained by calling the function
		// skip(n, s, s) defined above, where n is the number of pixels in a full resolution frame and s is the number of
		// iterations (skip(256, 4, 4) = 340).
		//
		// Other offsets above are obtained with skip(256, 4, 3) = 84, skip(256, 4, 2) = 20 and skip(256, 4, 1) = 4. The
		// subsequent work takes a full resolution frame and compute a half resolution frame. We store the full frame in
		// output memory at offset 84 with a memcpy over 256 pixels; this is done before the loop starts.
		// In the loop body, we read input data from the previously computed/copied frame at some offset and put it in the
		// buffer in its immediate left. At the first iteration, we therefore read data from offset skip(256, 4, 4 - 1) = 84
		// along 256 / 4^0 = 256 pixels and we write it into offset skip(256, 4, 4 - 1 - 1) = 20 for 256 / 4^1 = 64 pixels. In
		// the second iteration, we read 256 / 4^1 pixels at offset skip(256, 4, 4 - 2 = 20 and we store all 256 / 4^2 = 16
		// pixels at offset skip(256, 4, 4 - 2 - 1). Finally, in the last iteration, we read 256 / 4^2 = 16 pixels at offset
		// skip(256, 4, 4 - 3) = 4 and we write 256 / 4^3 = 4 pixels at offset skip(256, 4, 4 - 3 - 1) = 0.
		// Finally, we can commit all frame output token to the next consumer task
		
		// a bigger output buffer that can hold the full resolution frame as well as all
		// successive lower resolution frames.
		// Compute first round and write it as second last token in output buffer
		size_t offset = skip(inSize, args->pyramid.size(), args->pyramid.size() - 1);

		size_t length_x = args->compSize.x;
		size_t length_y = args->compSize.y;

	if(output_size > outSize)
	{
		//pelib_cfifo_fill(int)(output_link->buffer, output_size);
		drake_output_commit(output)(output_size - outSize);
		output = drake_output_buffer(output)(&output_size, NULL);
	}
		memcpy(output + offset, input, length_x * length_y * sizeof(float));
		for(size_t i = 1; i < args->pyramid.size(); i++)
		{
			size_t read_offset = skip(inSize, args->pyramid.size(), args->pyramid.size() - i);
			size_t write_offset = skip(inSize, args->pyramid.size(), args->pyramid.size() - i - 1);
			length_x = args->compSize.x / pow(2, i - 1);
			length_y = args->compSize.y / pow(2, i - 1);
			uint2 length = make_uint2(args->compSize.x / pow(2, i - 1), args->compSize.y / pow(2, i - 1));

			// At this point, we can compute the first loop iteration from the second last token computed above
//	drake_platform_time_get(start);
#if PARALLEL_HALF_SAMPLE
			struct half_sample_args pargs;
			pargs.output = output + write_offset;
			pargs.input = output + read_offset;
			pargs.length = length;
			pargs.args = args;
			drake_task_pool_run(parallel_half_sample, &pargs);
#else
			halfSampleRobustImageKernel(output + write_offset, output + read_offset, length, args->e_d * 3, 1, 0, 1);
#endif
//	drake_platform_time_get(stop);
		}

		// Now copy the last output token, i.e., the unmodified frame, to output after all reduced resolution token written before through iterations above.
		//pelib_cfifo_fill(int)(output_link->buffer, outSize);
		drake_output_commit(output)(outSize);

		// Discard input
		//pelib_cfifo_discard(int)(input_link->buffer, inSize);
		drake_input_discard(input)(inSize);
/*
	drake_platform_time_subtract(duration, stop, start);
	printf("half sample: ");
	drake_platform_time_printf(stdout, duration);
	printf("\n");
*/
	}
	else
	{
		// Either input channel is empty, either it is full of garbage because
		// of buffer wrapping performed by last task. In the latter case, we
		// should get rid of it. In the former case, we just discard 0 element.
	}

	int res = drake_task_autoexit();
	//debug(res);
	return res;
}

int
drake_kill()
{
	//debug("half_sample");
	//drake_platform_time_get(killed[task->id - 1]);

	// Do nothing
	return 0;
}

int
drake_destroy()
{
	// Do nothing
	return 1;
}
