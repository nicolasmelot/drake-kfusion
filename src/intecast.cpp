/*
 Copyright 2015 Nicolas Melot

 This file is part of Drake-merge.

 Drake-merge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Drake-merge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Drake-merge. If not, see <http://www.gnu.org/licenses/>.

*/

#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <drake.h>
#include <drake/link.h>
#include <string.h>

#include <drake-kfusion.h> 
#include "kernels.h"
#include "util.h"

drake_declare_input(reduction, float);
drake_declare_input(pose, float);
drake_declare_input(depth, float);
drake_declare_output(normal, float);
drake_declare_output(pose, float);
drake_declare_output(vertex, float);

static drake_kfusion_t *args;
static size_t i = 0, j = 0;
static size_t frame = 0;
static float oldpose[16];
//static drake_time_t start, stop, integrate, raycast;
//static link_tp reduction_link, normal_link, output_pose_link, pose_link, vertex_link, depth_link;

int
drake_init(void* arg)
{
	/*
	reduction_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "reduction")).value;
	normal_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "normal")).value;
	output_pose_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "pose")).value;
	pose_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "pose")).value;
	vertex_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "vertex")).value;
	depth_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "depth")).value;
	*/
	args = (drake_kfusion_t*)arg;
/*
	start = drake_platform_time_alloc();
	stop = drake_platform_time_alloc();
	integrate = drake_platform_time_alloc();
	raycast = drake_platform_time_alloc();
*/
	i = 0;
	j = 0;
	frame = 0;

	memcpy(oldpose, &args->pose, sizeof(float) * 16);
	return 1;
}

int
drake_start()
{
	// Boostrap output link toward tracking
	/*
	link_tp ref_vertex_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "vertex")).value;
	link_tp ref_normal_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "normal")).value;
	link_tp output_pose_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "pose")).value;
	*/
	//float *vertex_wrap;
	//vertex = (float*)pelib_cfifo_writeaddr(int)(vertex_link->buffer, &vertex_size, &vertex_wrap);
	size_t depthSize = args->compSize.x * args->compSize.y;
	size_t vertex_size = 0;
	float *vertex = drake_output_buffer(vertex)(&vertex_size, NULL);
	if(vertex_size > depthSize * 3)
	{
		//pelib_cfifo_fill(int)(vertex_link->buffer, vertex_size);
		drake_output_commit(vertex)(vertex_size - depthSize * 3);
		vertex = drake_output_buffer(vertex)(&vertex_size, NULL);
	}

	//float *normal_wrap;
	//normal = (float*)pelib_cfifo_writeaddr(int)(normal_link->buffer, &normal_size, &normal_wrap);
	size_t normal_size = 0;
	float *normal = drake_output_buffer(normal)(&normal_size, NULL);
	if(normal_size > depthSize * 3)
	{
		//pelib_cfifo_fill(int)(normal_link->buffer, normal_size);
		drake_output_commit(normal)(normal_size - depthSize * 3);
		normal = drake_output_buffer(normal)(&normal_size, NULL);
	}
	//float* ref_vertex = (float*)pelib_cfifo_writeaddr(int)(ref_vertex_link->buffer, &ref_vertex_size, NULL);
	if(vertex_size >= 3 * args->compSize.x * args->compSize.y)
	{
		memset(vertex, 0, sizeof(float) * 3 * args->compSize.x * args->compSize.y); // / pow(4, args->pyramid.size() - 1));
	}
	else
	{
		debug("Output buffer is too small");
		debug("Intecast(vertex) -> track(refvertex)");
		debug(vertex_size * sizeof(float));
		debug(3 * args->compSize.x * args->compSize.y * sizeof(float));
		abort();
	}
	/*
	char *ptr = (char*)ref_vertex;
	for(int i = 0; i < sizeof(float) * 3 * args->compSize.x * args->compSize.y; i++)
	{
		ptr[i] = 0;
	}
	*/
	//pelib_cfifo_fill(int)(ref_vertex_link->buffer, 3 * args->compSize.x * args->compSize.y);
	drake_output_commit(vertex)(3 * args->compSize.x * args->compSize.y);

	//float* ref_normal = (float*)pelib_cfifo_writeaddr(int)(ref_normal_link->buffer, &ref_normal_size, NULL);
	memset(normal, 0, sizeof(float) * 3 * args->compSize.x * args->compSize.y); // / pow(4, args->pyramid.size() - 1) * sizeof(float));
	//pelib_cfifo_fill(int)(ref_normal_link->buffer, 3 * args->compSize.x * args->compSize.y);
	drake_output_commit(normal)(3 * args->compSize.x * args->compSize.y);

	size_t output_pose_size = 0;
	//float* output_pose = (float*)pelib_cfifo_writeaddr(int)(output_pose_link->buffer, &output_pose_size, NULL);
	float *output_pose = drake_output_buffer(pose)(&output_pose_size, NULL);
	memset(output_pose, 0, 16 * sizeof(float)); // / pow(4, args->pyramid.size() - 1));
	//debug(output_pose);
	//pelib_cfifo_fill(int)(output_pose_link->buffer, 16);
	//debug(16);
	//debug(drake_output_available(pose));
	drake_output_commit(pose)(16);
	//debug(drake_output_available(pose));

	return 1;
}

#define PARALLEL_INTEGRATE 1
#define PARALLEL_RAYCAST 1
#define PARALLEL_RENDER 1
#if PARALLEL_INTEGRATE
struct integrate_args
{
	drake_kfusion_t *args;
	float *depth, *pose;
};

static int
parallel_integrate(void *aux)
{
	struct integrate_args *args = (struct integrate_args*)aux;
	integrateKernel(*(Volume*)args->args->volume, args->depth, args->args->compSize, inverse(*(Matrix4*)args->pose), getCameraMatrix(args->args->k), args->args->mu, args->args->maxweight, drake_task_core_id(), drake_task_width());

	return 1;
}
#endif

#if PARALLEL_RAYCAST
struct raycast_args
{
	drake_kfusion_t *args;
	float *vertex, *normal;
	Matrix4 *pose4;
};

static int
parallel_raycast(void *aux)
{
	struct raycast_args *args = (struct raycast_args*)aux;
	drake_raycastKernel((float3*)args->vertex, (float3*)args->normal, args->args->compSize, *(Volume*)args->args->volume, *args->pose4, args->args->nearPlane, args->args->farPlane, args->args->step, 0.75f * args->args->mu, drake_task_core_id(), drake_task_width());

	return 1;
}
#endif

#if PARALLEL_RENDER
struct render_args
{
	drake_kfusion_t *args;
	uchar4 *buffer;
	Volume *vol;
};

static int
parallel_render(void *aux)
{
	struct render_args *args = (struct render_args*)aux;
	renderVolumeKernel(args->buffer, args->args->compSize, *args->vol, (**args->args->viewPose) * getInverseCameraMatrix(args->args->k), args->args->nearPlane, args->args->farPlane * 2.0f, args->args->step, args->args->largestep, args->args->light, args->args->ambient, drake_task_core_id(), drake_task_width());

	return 1;
}
#endif

int
drake_run()
{
	static int integrated = 0;
	size_t reduction_size = 0;
	//float* reduction = (float*)pelib_cfifo_peekaddr(int)(reduction_link->buffer, 0, &reduction_size, NULL);
	float *reduction = drake_input_buffer(reduction)(0, &reduction_size, NULL);
	
	size_t depth_size = 0;
	//float* depth = (float*)pelib_cfifo_peekaddr(int)(depth_link->buffer, 0, &depth_size, NULL);
	float *depth = drake_input_buffer(depth)(0, &depth_size, NULL);

	size_t pose_size = 0;
	//float* pose = (float*)pelib_cfifo_peekaddr(int)(pose_link->buffer, 0, &pose_size, NULL);
	float *pose = drake_input_buffer(pose)(0, &pose_size, NULL);

	size_t vertex_size;
	//float *vertex = (float*)pelib_cfifo_writeaddr(int)(vertex_link->buffer, &vertex_size, NULL);
	float *vertex = drake_output_buffer(vertex)(&vertex_size, NULL);
	
	size_t normal_size;
	//float *normal = (float*)pelib_cfifo_writeaddr(int)(normal_link->buffer, &normal_size, NULL);
	float *normal = drake_output_buffer(normal)(&normal_size, NULL);

	size_t output_pose_size = 0;
	//float* output_pose = (float*)pelib_cfifo_writeaddr(int)(output_pose_link->buffer, &output_pose_size, NULL);
	float *output_pose = drake_output_buffer(pose)(&output_pose_size, NULL);

	size_t depthSize = args->compSize.x * args->compSize.y;
	size_t reductionSize = 8 * 32;
	size_t poseSize = 16;
	int frame_ok = 0;

#if DEBUG_INCONSISTENCIES
	size_t vertexSize = 3 * depthSize;
	size_t normalSize = 3 * depthSize;
	if(
		/*
		   pelib_cfifo_capacity(int)(reduction_link->buffer) < reductionSize
		|| pelib_cfifo_capacity(int)(depth_link->buffer) < depthSize
		|| pelib_cfifo_capacity(int)(pose_link->buffer) < poseSize
		|| pelib_cfifo_capacity(int)(vertex_link->buffer) < vertexSize
		|| pelib_cfifo_capacity(int)(normal_link->buffer) < normalSize
		|| pelib_cfifo_capacity(int)(output_pose_link->buffer) < 16 * 2 // We need an old and a new pose to live together
		*/
		   drake_input_capacity(reduction) < reductionSize
		|| drake_input_capacity(depth) < depthSize
		|| drake_input_capacity(pose) < poseSize
		|| drake_output_capacity(vertex) < vertexSize
		|| drake_output_capacity(normal) < normalSize
		|| drake_output_capacity(pose) < poseSize * 2
	)
	{
		const char *buffer;
		size_t buffer_expected;
		size_t buffer_actual;
		//if(pelib_cfifo_capacity(int)(reduction_link->buffer) < reductionSize)
		if(drake_input_capacity(reduction) < reductionSize)
		{
			buffer = "reduction";
			buffer_expected = reductionSize;
			//buffer_actual = pelib_cfifo_capacity(int)(reduction_link->buffer);
			buffer_actual = drake_input_capacity(reduction);
		}
		//else if(pelib_cfifo_capacity(int)(depth_link->buffer) < depthSize)
		else if(drake_input_capacity(depth) < depthSize)
		{
			buffer = "depth";
			buffer_expected = depthSize;
			//buffer_actual = pelib_cfifo_capacity(int)(depth_link->buffer);
			buffer_actual = drake_input_capacity(depth);
		}
		//else if(pelib_cfifo_capacity(int)(pose_link->buffer) < poseSize)
		if(drake_input_capacity(pose) < depthSize)
		{
			buffer = "pose";
			buffer_expected = poseSize;
			//buffer_actual = pelib_cfifo_capacity(int)(pose_link->buffer);
			buffer_actual = drake_input_capacity(pose);
		}
		//else if(pelib_cfifo_capacity(int)(vertex_link->buffer) < depthSize)
		else if(drake_output_capacity(vertex) < depthSize)
		{
			buffer = "vertex";
			buffer_expected = depthSize * 3;
			//buffer_actual = pelib_cfifo_capacity(int)(vertex_link->buffer);
			buffer_actual = drake_output_capacity(vertex);
		}
		//else if(pelib_cfifo_capacity(int)(normal_link->buffer) < depthSize)
		else if(drake_output_capacity(normal) < depthSize)
		{
			buffer = "normal";
			buffer_expected = depthSize * 3;
			//buffer_actual = pelib_cfifo_capacity(int)(normal_link->buffer);
			buffer_actual = drake_output_capacity(normal);
		}
		//else if(pelib_cfifo_capacity(int)(output_pose_link->buffer) < 16 * sizeof(float) * 2)
		else if(drake_output_capacity(pose) < depthSize)
		{
			buffer = "output pose";
			buffer_expected = 16 * 2;
			//buffer_actual = pelib_cfifo_capacity(int)(output_pose_link->buffer);
			buffer_actual = drake_output_capacity(pose);
		}

		printf("[%s:%s:%d] %s buffer cannot fit input or output frame (%zu > %zu)\n", __FILE__, __FUNCTION__, __LINE__, buffer, buffer_expected, buffer_actual);
		abort();
	}
#endif

	if(reduction_size > reductionSize)
	{
		//pelib_cfifo_discard(int)(reduction_link->buffer, reduction_size);
		drake_input_discard(reduction)(reduction_size - reductionSize);
		reduction = drake_input_buffer(reduction)(0, &reduction_size, NULL);
	}
	if(depth_size > depthSize)
	{
		//pelib_cfifo_discard(int)(depth_link->buffer, depth_size);
		drake_input_discard(depth)(depth_size - depthSize);
		depth = drake_input_buffer(depth)(0, &depth_size, NULL);
	}
	if(pose_size > poseSize)
	{
		//pelib_cfifo_discard(int)(pose_link->buffer, pose_size);
		drake_input_discard(pose)(pose_size - poseSize);
		pose = drake_input_buffer(pose)(0, &pose_size, NULL);
	}
	//float *vertex_wrap;
	//vertex = (float*)pelib_cfifo_writeaddr(int)(vertex_link->buffer, &vertex_size, &vertex_wrap);

	//float *normal_wrap;
	//normal = (float*)pelib_cfifo_writeaddr(int)(normal_link->buffer, &normal_size, &normal_wrap);
#define DEBUG_GUARD_INTEGRATE 0
#if DEBUG_GUARD_INTEGRATE
debug("####### Integrate #########");
debug(integrated);
debug(reduction_size);
debug(pose_size);
debug(depth_size);
debug("########## End ############");
#endif
	if(
		   integrated == 0
		&& reduction_size >= reductionSize
		&& pose_size >= poseSize
		&& depth_size >= depthSize
	)
	{
		int doIntegrate = checkPoseKernel(*(Matrix4*)pose, *(Matrix4*)oldpose, reduction, args->compSize, args->track_threshold);
		if ((doIntegrate && ((frame % args->integration_rate) == 0)) || (frame <= 3))
		{
//	drake_platform_time_get(start);
#if PARALLEL_INTEGRATE
			struct integrate_args pargs;
			pargs.args = args;
			pargs.pose = pose;
			pargs.depth = depth;
			drake_task_pool_run(parallel_integrate, &pargs);
#else
			integrateKernel(*(Volume*)args->volume, depth, args->compSize, inverse(*(Matrix4*)pose), getCameraMatrix(args->k), args->mu, args->maxweight, 0, 1);
#endif
//	drake_platform_time_get(stop);

			// Consume depth
			i = (i + 1) % args->pyramid.size();
		}

		// Do render
		if(args->render_volume != 0 && ((frame % args->rendering_rate) == 0))
		{
			//debug("Render Volume");
			uchar4 *buffer = (uchar4*)args->render_volume_buffer;
#if PARALLEL_RENDER
			render_args pargs;
			pargs.buffer = buffer;
			pargs.args = args;
			pargs.vol = args->volume;
			drake_task_pool_run(parallel_render, &pargs);
#else
			Volume &vol = *(Volume*)args->volume;
			uint2 compSize = make_uint2(args->compSize.x, args->compSize.y);
			renderVolumeKernel(buffer, compSize, vol, (**args->viewPose) * getInverseCameraMatrix(args->k), args->nearPlane, args->farPlane * 2.0f, args->step, args->largestep, args->light, args->ambient);
#endif
		}
		sem_post(&args->render_volume_done);

		//pelib_cfifo_discard(int)(depth_link->buffer, depthSize);
		//debug("discard");
		drake_input_discard(depth)(depthSize);
		//pelib_cfifo_discard(int)(reduction_link->buffer, reductionSize);
		drake_input_discard(reduction)(reductionSize);
		// Integration is done
		integrated = 1;
		frame_ok = 1;
/*
	drake_platform_time_subtract(integrate, stop, start);
	printf("integrate: ");
	drake_platform_time_printf(stdout, integrate);
	printf("\n");
*/
	}
	else
	{
		// Either input channel is empty, either it is full of garbage because
		// of buffer wrapping performed by last task. In the latter case, we
		// should get rid of it. In the former case, we just discard 0 element.
	}

#define DEBUG_GUARD_RAYCAST 0
#if DEBUG_GUARD_RAYCAST
debug("######## Raycast ##########");
debug(integrated);
debug(pose_size);
debug(vertex_size);
debug(normal_size);
debug(frame);
debug("########## End ############");
#endif
	if(
		   integrated != 0
		&& pose_size >= poseSize
		&& (
			(
				   vertex_size >= depthSize * 3
				&& normal_size >= depthSize * 3
			)
			|| frame <= 2
		)
	)
	{
		if (frame > 2)
		{
			Matrix4 pose4 = *(Matrix4*)pose;
			pose4 = pose4 * getInverseCameraMatrix(args->k);

			// Forward pose to track
			// If there is not enough room for pushing a pose
			// Then we must have reached the end of fifo and we need to
			// fill we garbage before sending a new pose
			if(output_pose_size < 16)
			{
				//pelib_cfifo_fill(int)(output_pose_link->buffer, output_pose_size);
				drake_output_commit(pose)(output_pose_size);
				//output_pose = (float*)pelib_cfifo_writeaddr(int)(output_pose_link->buffer, &output_pose_size, NULL);
				output_pose = drake_output_buffer(pose)(&output_pose_size, NULL);
			}
#if DEBUG_INCONSISTENCIES
			if(output_pose_size < 16)
			{
				debug("This should never happen");
				abort();
			}
#endif
			memcpy(output_pose, pose, 16 * sizeof(float));
			memcpy(*args->viewPose, pose, 16 * sizeof(float));
			//pelib_cfifo_fill(int)(output_pose_link->buffer, 16);
			drake_output_commit(pose)(poseSize);

	if(vertex_size > depthSize * 3)
	{
		//pelib_cfifo_fill(int)(vertex_link->buffer, vertex_size);
		drake_output_commit(vertex)(vertex_size - depthSize * 3);
		vertex = drake_output_buffer(vertex)(&vertex_size, NULL);
	}
	if(normal_size > depthSize * 3)
	{
		//pelib_cfifo_fill(int)(normal_link->buffer, normal_size);
		drake_output_commit(normal)(normal_size - depthSize * 3);
		normal = drake_output_buffer(normal)(&normal_size, NULL);
	}
#if PARALLEL_RAYCAST
			struct raycast_args pargs;
			pargs.normal = normal;
			pargs.vertex = vertex;
			pargs.pose4 = &pose4;
			pargs.args = args;
			drake_task_pool_run(parallel_raycast, &pargs);
#else
//			drake_platform_time_get(start);
			drake_raycastKernel((float3*)vertex, (float3*)normal, args->compSize, *(Volume*)args->volume, pose4, args->nearPlane, args->farPlane, args->step, 0.75f * args->mu, 0, 1);
#endif
//	drake_platform_time_get(stop);
			frame_ok = 1;

			// produce vertex
			//pelib_cfifo_fill(int)(vertex_link->buffer, depthSize * 3);
			drake_output_commit(vertex)(depthSize * 3);
			// produce normal
			//pelib_cfifo_fill(int)(normal_link->buffer, depthSize * 3);
			drake_output_commit(normal)(depthSize * 3);
		}

		// Get a backup of former pose
		memcpy(oldpose, pose, sizeof(float) * 16);
		// consume pose
		//pelib_cfifo_discard(int)(pose_link->buffer, poseSize);
		drake_input_discard(pose)(poseSize);

		// Count frames received
		frame++;

		// Enable integration of next frame
		integrated = 0;

/*
	drake_platform_time_subtract(raycast, stop, start);
	printf("raycast: ");
	drake_platform_time_printf(stdout, raycast);
	printf("\n");
*/
	}
	else
	{
	}

	// Wrap buffers around

	if(frame_ok)
	{
		sem_post(&args->frame_output);
	}

	int res = drake_task_autoexit();
	//debug(res);
	return res;
}

int
drake_kill()
{
	//debug("intecast");
	//drake_platform_time_get(killed[task->id - 1]);

	// Do nothing
	return 0;
}

int
drake_destroy()
{
	// Do nothing
	return 1;
}

