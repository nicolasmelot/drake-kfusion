#ifndef DRAKE_KERNELS_H
#define DRAKE_KERNELS_H 1

#ifdef __cplusplus
//extern "C" {
#endif

#include <kernels.h>
#include <stdlib.h>

typedef struct {
	float x;
	float y;
	float z;
} c_float3;
typedef struct {
	float x;
	float y;
	float z;
	float w;
} c_float4;

typedef struct {
	unsigned int x;
	unsigned int y;
} c_uint2;

typedef struct {
	unsigned int x;
	unsigned int y;
	unsigned int z;
} c_uint3;

/*
void* float16toMatrix4ptr(const float* float16);
void matrix4ToFloat16(float *float16, const void *ptr)
*/


void mm2metersKernel(float * out, uint2 outSize, const ushort * in, uint2 inSize, unsigned int core, unsigned int width);
//void bilateralFilterKernel(float* out, const float* in, size_t size_x, size_t size_y, const float * gaussian, float e_d, int r);
void bilateralFilterKernel(float* out, const float* in, uint2 size, const float * gaussian, float e_d, int r, unsigned int core, unsigned int width);
//void halfSampleRobustImageKernel(float* out, const float* in, size_t inSize_x, size_t inSize_y, const float e_d, const int r);
void halfSampleRobustImageKernel(float* out, const float* in, uint2 inSize, const float e_d, const int r, unsigned int core, unsigned int width);
//void depth2vertexKernel(float* vertex, const float * depth, size_t imageSize_x, size_t imageSize_y, const float invK[16]);
void depth2vertexKernel(float3* vertex, const float * depth, uint2 imageSize, const Matrix4 invK, unsigned int core, unsigned int width);
//void vertex2normalKernel(float * out, const float * in, size_t imageSize_x, size_t imageSize_y);
void vertex2normalKernel(float3 * out, const float3 * in, uint2 imageSize, unsigned int core, unsigned int width);
//void trackKernel(float * output, float* inVertex, float* inNormal, size_t inSize_x, size_t inSize_y, float* refVertex, float* refNormal, size_t refSize_x, size_t refSize_y, float Ttrack[16], float view[16], float dist_threshold, float normal_threshold);
void trackKernel(TrackData* output, const float3* inVertex, const float3* inNormal, uint2 inSize, const float3* refVertex, const float3* refNormal, uint2 refSize, const Matrix4 Ttrack, const Matrix4 view, const float dist_threshold, const float normal_threshold, unsigned int core, unsigned int width);
//void reduceKernel(float * out, float *J, const uint Jsize_x, const uint Jsize_y, const uint size_x, const uint size_y);
void reduceKernel(float * out, TrackData* J, const uint2 Jsize, const uint2 size, unsigned int core, unsigned int width);
//void integrateKernel(void *vol, const float* depth, size_t depthSize_x, size_t depthSize_y, const float* invTrack, const float* K, const float mu, const float maxweight);
void integrateKernel(Volume vol, const float* depth, uint2 depthSize, const Matrix4 invTrack, const Matrix4 K, const float mu, const float maxweight, unsigned int core, unsigned int width);
//void raycastKernel(float* vertex, float* normal, unsigned int inputSize_x, unsigned int inputSize_y, const void *integration, const float* view, const float nearPlane, const float farPlane, const float step, const float largestep);
void drake_raycastKernel(float3* vertex, float3* normal, uint2 inputSize, const Volume integration, const Matrix4 view, const float nearPlane, const float farPlane, const float step, const float largestep, unsigned int core, unsigned int width);
//int updatePoseKernel(float *posep, const float * output, float icp_threshold);
bool updatePoseKernel(Matrix4 & pose, const float * output, float icp_threshold);
//int checkPoseKernel(float *posep, float *oldPosep, const float * output, size_t imageSize_x, size_t imageSize_y, float track_threshold);
bool checkPoseKernel(Matrix4 & pose, Matrix4 oldPose, const float * output, uint2 imageSize, float track_threshold);
//void renderDepthKernel(void *out, float *depth, size_t depthSize_x, size_t depthSize_y, const float nearPlane, const float farPlane);
void renderDepthKernel(uchar4* out, float * depth, uint2 depthSize, const float nearPlane, const float farPlane, unsigned int core, unsigned int width);
//void renderTrackKernel(void *out, const float *data_ptr, size_t outSize_x, size_t outSize_y);
void renderTrackKernel(uchar4* out, const TrackData* data, uint2 outSize, unsigned int core, unsigned int width);
//void renderVolumeKernel(void* out, const size_t depthSize_x, const size_t depthSize_y, const void *vol, const void *init_view_ptr, const float camera_ptr[4], const float nearPlane, const float farPlane, const float step, const float largestep, const void *light, const void *ambient);
void renderVolumeKernel(uchar4* out, const uint2 depthSize, const Volume volume, const Matrix4 view, const float nearPlane, const float farPlane, const float step, const float largestep, const float3 light, const float3 ambient, unsigned int core, unsigned int width);

#ifdef __cplusplus
//}
#endif
#endif
