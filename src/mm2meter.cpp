/*
 Copyright 2015 Nicolas Melot

 This file is part of Drake-merge.

 Drake-merge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Drake-merge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Drake-merge. If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <math.h>

#include <drake.h>
#include <drake/link.h>
#include <string.h>

#include <drake-kfusion.h>
#include "kernels.h"
#include "util.h"

#ifdef debug
#undef debug
#endif

#define COLOR KNRM
#define debug(var) debug_color(var, COLOR)

drake_declare_output(output, float);
drake_declare_output(intecast, float);
//static drake_time_t start, stop, duration;

//static link_tp output_link, intecast_link;

static drake_kfusion_t *args;

int
drake_init(void* arg)
{
	/*
	output_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "output")).value;
	intecast_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "intecast")).value;
	*/

/*
	start = drake_platform_time_alloc();
	stop = drake_platform_time_alloc();
	duration = drake_platform_time_alloc();
*/
	args = (drake_kfusion_t*)arg;
	return 1;
}

int
drake_start()
{
	return 1;
}

#define PARALLEL_MM2METERS 1
#if PARALLEL_MM2METERS
struct mm2meters_args
{
	float *output;
	drake_kfusion_t *args;
};

static int
parallel_mm2meters(void *aux)
{
	struct mm2meters_args *args = (struct mm2meters_args*)aux;
	mm2metersKernel(args->output, args->args->compSize, args->args->in, args->args->inSize, drake_task_core_id(), drake_task_width());
	
	return 1;
}
#endif

int
drake_run()
{
	// Prepare output link
	size_t output_size;
	//float *output = (float*)pelib_cfifo_writeaddr(int)(output_link->buffer, &output_size, NULL);
	float *output = drake_output_buffer(output)(&output_size, NULL);

	// Prepare output link
	size_t intecast_size;
	//float *intecast = (float*)pelib_cfifo_writeaddr(int)(intecast_link->buffer, &intecast_size, NULL);
	float *intecast = drake_output_buffer(intecast)(&intecast_size, NULL);
	size_t outSize = args->compSize.x * args->compSize.y;

#if DEBUG_INCONSISTENCIES
	if(
		/*
		   pelib_cfifo_capacity(int)(output_link->buffer) < outSize
		|| pelib_cfifo_capacity(int)(intecast_link->buffer) < outSize
		*/
		   drake_output_capacity(output) < outSize
		|| drake_output_capacity(intecast) < outSize
	)
	{
		printf("[%s:%s:%d] Output buffer cannot fit output frame\n", __FILE__, __FUNCTION__, __LINE__);
		abort();
	}
#endif


	// Wrap buffer around if remainder memory is too small
	
	int value;
	int sleep = 0;
	sem_wait(&args->frame_queue);
	value = 1;
	//sem_getvalue(&args->frame_queue, &value);
#define DEBUG_GUARD 0
#if DEBUG_GUARD
debug("####### MM2Mmeters ########");
debug(outSize);
debug(output_size);
debug(intecast_size);
debug(value);
debug("########## End ############");
#endif
	if(
		   output_size >= outSize
		&& intecast_size >= outSize
		&& value != 0
		&& args->no_more_frame == 0
	)
	{
		//pelib_cfifo_fill(int)(output_link->buffer, outSize);
	if(output_size < outSize)
	{
		//pelib_cfifo_fill(int)(output_link->buffer, output_size);
		drake_output_commit(output)(output_size - outSize);
		output = drake_output_buffer(output)(&output_size, NULL);
	}
//	drake_platform_time_get(start);
#if PARALLEL_MM2METERS
	struct mm2meters_args pargs;
	pargs.output = output;
	pargs.args = args;
	drake_task_pool_run(parallel_mm2meters, &pargs);
#else
	mm2metersKernel(output, args->compSize, args->in, args->inSize, 0, 1);
#endif
//	drake_platform_time_get(stop);
		//pelib_cfifo_fill(int)(intecast_link->buffer, outSize);
	if(intecast_size < outSize)
	{
		//pelib_cfifo_fill(int)(intecast_link->buffer, intecast_size);
		drake_output_commit(output)(intecast_size - outSize);
		intecast = drake_output_buffer(intecast)(&intecast_size, NULL);
	}

		memcpy(intecast, output, sizeof(float) * outSize);
		drake_output_commit(output)(outSize);
		//debug("commit");
		drake_output_commit(intecast)(outSize);

		//sem_wait(&args->frame_queue);
		sem_post(&args->frame_consume);

/*
	drake_platform_time_subtract(duration, stop, start);
	printf("mm2meter: ");
	drake_platform_time_printf(stdout, duration);
	printf("\n");
*/
	}
	else
	{
	}
	sleep = 1;
	
	//output = (float*)pelib_cfifo_writeaddr(int)(output_link->buffer, &output_size, &output_wrap);
	//debug(args->no_more_frame);
	int exit = drake_task_kill(args->no_more_frame) | drake_task_sleep(sleep);
	//debug(exit);
	return exit;
}

int
drake_kill()
{
	//debug("mm2meters");
	//drake_platform_time_get(killed[task->id - 1]);

	// Do nothing
	return 0;
}

int
drake_destroy()
{
	// Do nothing
	return 1;
}
