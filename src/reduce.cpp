/*
 Copyright 2015 Nicolas Melot

 This file is part of Drake-merge.

 Drake-merge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Drake-merge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Drake-merge. If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <drake.h>
#include <drake/link.h>
#include <string.h>

#include <drake-kfusion.h> 
#include "kernels.h"
#include "util.h"

drake_declare_input(input, float);
drake_declare_input(i, int);
drake_declare_input(j, int);
drake_declare_output(update, float);

static drake_kfusion_t *args;
//static drake_time_t start, stop, duration;
//static link_tp input_link, i_link, update_link;

int
drake_init(void* arg)
{
	/*
	input_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "input")).value;
	i_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "i")).value;
	update_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "update")).value;
	*/
	args = (drake_kfusion_t*)arg;
/*
	start = drake_platform_time_alloc();
	stop = drake_platform_time_alloc();
	duration = drake_platform_time_alloc();
*/
	return 1;
}

int
drake_start()
{
	return 1;
}

#define PARALLEL_REDUCE 1
#if PARALLEL_REDUCE
struct reduce_args
{
	drake_kfusion_t *args;
	float *update, *input;
	uint2 inSize2;
};

static int
parallel_reduce(void *aux)
{
	struct reduce_args *args = (struct reduce_args*)aux;
	reduceKernel(args->update, (TrackData*)args->input, args->args->compSize, args->inSize2, drake_task_core_id(), drake_task_width());

	return 1;
}
#endif

int
drake_run()
{
	// Fetch links

	// Fetch buffer addresses
	size_t input_size = 0;
	//float* input = (float*)pelib_cfifo_peekaddr(int)(input_link->buffer, 0, &input_size, NULL);
	float *input = drake_input_buffer(input)(0, &input_size, NULL);

	size_t i_size = 0;
	//int* i_ptr = pelib_cfifo_peekaddr(int)(i_link->buffer, 0, &i_size, NULL);
	int* i_ptr = drake_input_buffer(i)(0, &i_size, NULL);
	//size_t j_size = 0;
	//int* j_ptr = drake_input_buffer(j)(0, &j_size, NULL);
	size_t update_size;
	//float *update = (float*)pelib_cfifo_writeaddr(int)(update_link->buffer, &update_size, NULL);
	float *update = drake_output_buffer(update)(&update_size, NULL);

	int i = 0;
	if(i_size >= 1)
	{
		i = *i_ptr;
	}
	/*
	int j = 0;
	if(j_size >= 1)
	{
		j = *j_ptr;
	}
	*/

	size_t inSize_x = args->compSize.x / pow(2, args->pyramid.size() - i - 1);
	size_t inSize_y = args->compSize.y / pow(2, args->pyramid.size() - i - 1);
	size_t inSize = 8 * args->compSize.x * args->compSize.y / pow(4, args->pyramid.size() - i - 1);
	size_t outSize = 8 * 32; // 8 * 32 output matrix

#if DEBUG_INCONSISTENCIES
	if(
		//   pelib_cfifo_capacity(int)(input_link->buffer) < inSize
		//|| pelib_cfifo_capacity(int)(update_link->buffer) < outSize
		   drake_input_capacity(input) < inSize
		|| drake_output_capacity(update) < outSize
	)
	{
		const char *buffer;
		size_t buffer_expected;
		size_t buffer_actual;
		//if(pelib_cfifo_capacity(int)(input_link->buffer) < inSize)
		if(drake_input_capacity(input) < inSize)
		{
			buffer = "input";
			buffer_expected = inSize;
			//buffer_actual = pelib_cfifo_capacity(int)(input_link->buffer);
			buffer_actual = drake_input_capacity(input);
		}
		//else if(pelib_cfifo_capacity(int)(update_link->buffer) < outSize)
		else if(drake_output_capacity(update) < outSize)
		{
			buffer = "update";
			buffer_expected = outSize;
			//buffer_actual = pelib_cfifo_capacity(int)(update_link->buffer);
			buffer_actual = drake_output_capacity(update);
		}

		printf("[%s:%s:%d] %s buffer cannot fit input or output frame (%zu > %zu)\n", __FILE__, __FUNCTION__, __LINE__, buffer, buffer_expected, buffer_actual);
		abort();
	}
#endif

	if(input_size > inSize)
	{
		//pelib_cfifo_discard(int)(input_link->buffer, input_size);
		//debug(input_size);
		drake_input_discard(input)(input_size - inSize);
		input = drake_input_buffer(input)(0, &input_size, NULL);
		// Discard also i index
		//pelib_cfifo_pop(int)(i_link->buffer);
		//debug(1);
		//drake_input_discard(i)(1);
	}

	// Wrap buffer around if remainder memory is too small
	//float *update_wrap = NULL;
	//update = (float*)pelib_cfifo_writeaddr(int)(update_link->buffer, &update_size, NULL);
	//update = drake_output_buffer(update)(&update_size, &update_wrap);

#define DEBUG_GUARD 0
#if DEBUG_GUARD
debug("######### Reduce ##########");
debug(inSize);
debug(outSize);
debug(input_size);
debug(update_size);
debug(i_size);
//debug(j_size);
debug("########## End ############");
#endif
	if(
		   input_size >= inSize
		&& update_size >= outSize
		&& i_size >= 1
		//&& j_size >= 1
	)
	{
		uint2 inSize2 = make_uint2(inSize_x, inSize_y);
		//pelib_cfifo_fill(int)(update_link->buffer, outSize);
	if(update_size > outSize)
	{
		//pelib_cfifo_fill(int)(update_link->buffer, update_size);
		drake_output_commit(update)(update_size - outSize);
		update = drake_output_buffer(update)(&update_size, NULL);
	}
//	drake_platform_time_get(start);
#if PARALLEL_REDUCE
#warning Parallel reduce
	struct reduce_args pargs;
	pargs.update = update;
	pargs.input = input;
	pargs.args = args;
	pargs.inSize2 = inSize2;

	drake_task_pool_run(parallel_reduce, &pargs);
#else
#warning Sequential reduce
	reduceKernel(update, (TrackData*)input, args->compSize, inSize2, 0, 1);
#endif
//	drake_platform_time_get(stop);
		drake_output_commit(update)(outSize);

		// In any way, always discard input
		//pelib_cfifo_discard(int)(input_link->buffer, inSize);
		drake_input_discard(input)(inSize);
		//pelib_cfifo_discard(int)(i_link->buffer, 1);
		drake_input_discard(i)(1);
		//drake_input_discard(j)(1);
//	drake_platform_time_subtract(duration, stop, start);
//	printf("reduce: ");
//	drake_platform_time_printf(stdout, duration);
//	printf("\n");
	}

	//if(j == args->pyramid[args->pyramid.size() - i - 1])
	{
		//i++;
	}
	inSize = 8 * args->compSize.x * args->compSize.y / pow(4, args->pyramid.size() - i - 1);
	outSize = 8 * 32; // 8 * 32 output matrix
	{
		// Either input channel is empty, either it is full of garbage because
		// of buffer wrapping performed by last task. In the latter case, we
		// should get rid of it. In the former case, we just discard 0 element.
	}


	int res = drake_task_autoexit();
	//debug(res);
	return res;
}

int
drake_kill()
{
	//debug("reduce");
	//drake_platform_time_get(killed[task->id - 1]);

	// Do nothing
	return 0;
}

int
drake_destroy()
{
	// Do nothing
	return 1;
}
