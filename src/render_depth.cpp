/*
 Copyright 2015 Nicolas Melot

 This file is part of Drake-merge.

 Drake-merge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Drake-merge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Drake-merge. If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <math.h>

#include <drake.h>
#include <drake/link.h>
#include <string.h>

#include <drake-kfusion.h> 
#include "kernels.h"
#include "util.h"

drake_declare_input(input, float);
//static drake_time_t start, stop, duration;

static drake_kfusion_t *args;
//static link_tp input_link;

int
drake_init(void* arg)
{
	//input_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "input")).value;
	args = (drake_kfusion_t*)arg;
/*
	start = drake_platform_time_alloc();
	stop = drake_platform_time_alloc();
	duration = drake_platform_time_alloc();
*/

	// Initialization is always successful
	return 1;
}

int
drake_start()
{
	return 1;
}

#define PARALLEL_RENDER_DEPTH 1
#if PARALLEL_RENDER_DEPTH
struct render_depth_args
{
	drake_kfusion_t *args;
	void *out;
	float *depth;
	float nearPlane, farPlane;
};

static int
parallel_render_depth(void *aux)
{
	struct render_depth_args *args = (struct render_depth_args*)aux;
	renderDepthKernel((uchar4*)args->out, args->depth, args->args->compSize, args->nearPlane, args->farPlane, 0, 1);

	return 1;
}
#endif


int
drake_run()
{
	// Fetch buffer addresses
	size_t input_size = 0;
	//float* input = (float*)pelib_cfifo_peekaddr(int)(input_link->buffer, 0, &input_size, NULL);
	float *input = drake_input_buffer(input)(0, &input_size, NULL);

	size_t inSize = args->compSize.x * args->compSize.y;

#if DEBUG_INCONSISTENCIES
	//if(inSize > pelib_cfifo_capacity(int)(input_link->buffer))
	if(inSize > drake_input_capacity(input))
	{
		printf("[%s:%s:%d] %s buffer cannot fit input or output frame (%zu > %zu)\n", __FILE__, __FUNCTION__, __LINE__, "Input", inSize, drake_input_capacity(input));
		abort();
	}
#endif

	if(input_size > inSize)
	{
		//pelib_cfifo_discard(int)(input_link->buffer, input_size);
		drake_input_discard(input)(input_size - inSize);
		input = drake_input_buffer(input)(0, &input_size, NULL);
	}

	if(
		   input_size >= inSize
	)
	{
		void *out = args->render_depth_buffer;
		float *depth = input;
		float nearPlane = args->nearPlane;
		float farPlane = args->farPlane;
		//debug("Render Depth");

		if(args->render_depth != 0)
		{
//	drake_platform_time_get(start);
#if PARALLEL_RENDER_DEPTH
#warning Parallel render depth
			struct render_depth_args pargs;
			pargs.out = out;
			pargs.depth = depth;
			pargs.args = args;
			pargs.nearPlane = nearPlane;
			pargs.farPlane = farPlane;

			drake_task_pool_run(parallel_render_depth, &pargs);
#else
#warning Sequential render depth
			renderDepthKernel((uchar4*)out, depth, args->compSize, nearPlane, farPlane, 0, 1);
#endif
//	drake_platform_time_get(stop);
		}
		sem_post(&args->render_depth_done);

		// Discard input
		//pelib_cfifo_discard(int)(input_link->buffer, inSize);
		drake_input_discard(input)(inSize);
/*
	drake_platform_time_subtract(duration, stop, start);
	printf("render depth: ");
	drake_platform_time_printf(stdout, duration);
	printf("\n");
*/
	}
	else
	{
		// Either input channel is empty, either it is full of garbage because
		// of buffer wrapping performed by last task. In the latter case, we
		// should get rid of it. In the former case, we just discard 0 element.
	}

	int res = drake_task_autoexit();
	//debug(res);
	return res;
}

int
drake_kill()
{
	//drake_platform_time_get(killed[task->id - 1]);

	// Do nothing
	return 0;
}

int
drake_destroy()
{
	return 1;
}
