/*
 Copyright 2015 Nicolas Melot

 This file is part of Drake-merge.

 Drake-merge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Drake-merge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Drake-merge. If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <drake.h>
#include <drake/link.h>
#include <string.h>

#include <drake-kfusion.h> 
#include "kernels.h"
#include "util.h"

drake_declare_input(input, float);
//static drake_time_t start, stop, duration;

static drake_kfusion_t *args;
//link_tp input_link;

int
drake_init(void* arg)
{
	//input_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "input")).value;
	args = (drake_kfusion_t*)arg;
/*
	start = drake_platform_time_alloc();
	stop = drake_platform_time_alloc();
	duration = drake_platform_time_alloc();
*/
	return 1;
}

int
drake_start()
{
	return 1;
}

#define PARALLEL_RENDER_TRACK 1
#if PARALLEL_RENDER_TRACK
struct render_args
{
	drake_kfusion_t* args;
	float *input;
};

static int
parallel_render_track(void *aux)
{
	struct render_args *args = (struct render_args*)aux;
	renderTrackKernel((uchar4*)args->args->render_track_buffer, (const TrackData*)args->input, args->args->compSize, drake_task_core_id(), drake_task_width());

	return 1;
}
#endif

int
drake_run()
{
	// Fetch links

	// Fetch buffer addresses
	size_t input_size = 0;
	//float* input = (float*)pelib_cfifo_peekaddr(int)(input_link->buffer, 0, &input_size, NULL);
	float *input = drake_input_buffer(input)(0, &input_size, NULL);

	size_t inSize = 8 * args->compSize.x * args->compSize.y;

#if DEBUG_INCONSISTENCIES
	if(
		//   pelib_cfifo_capacity(int)(input_link->buffer) < inSize
		   drake_input_capacity(input) < inSize
	)
	{
		const char *buffer;
		size_t buffer_expected;
		size_t buffer_actual;
		//if(pelib_cfifo_capacity(int)(input_link->buffer) < inSize)
		if(drake_input_capacity(input) < inSize)
		{
			buffer = "input";
			buffer_expected = inSize;
			//buffer_actual = pelib_cfifo_capacity(int)(input_link->buffer);
			buffer_actual = drake_input_capacity(input);
		}

		printf("[%s:%s:%d] %s buffer cannot fit input or output frame (%zu > %zu)\n", __FILE__, __FUNCTION__, __LINE__, buffer, buffer_expected, buffer_actual);
		abort();
	}
#endif

#define DEBUG_GUARD 0
#if DEBUG_GUARD
debug("###### Render Track #######");
debug(inSize);
debug(input_size);
debug("########## End ############");
#endif
	if(input_size > inSize)
	{
		//pelib_cfifo_discard(int)(input_link->buffer, input_size);
		drake_input_discard(input)(input_size - inSize);
		input = drake_input_buffer(input)(0, &input_size, NULL);
	}

	if(
		   input_size >= inSize
	)
	{
		if(args->render_track != 0)
		{
//	drake_platform_time_get(start);
#if PARALLEL_RENDER_TRACK
#warning Parallel render track
		struct render_args pargs;
		pargs.input = input;
		pargs.args = args;

		drake_task_pool_run(parallel_render_track, &pargs);
#else
#warning Sequential render track
		renderTrackKernel((uchar4*)args->render_track_buffer, (const TrackData*)input, args->compSize, 0, 1);
#endif
//	drake_platform_time_get(stop);
		}
		sem_post(&args->render_track_done);
		//pelib_cfifo_discard(int)(input_link->buffer, inSize);
		//debug(inSize);
		drake_input_discard(input)(inSize);
/*
	drake_platform_time_subtract(duration, stop, start);
	printf("render track: ");
	drake_platform_time_printf(stdout, duration);
	printf("\n");
*/
	}
	else
	{
		// Either input channel is empty, either it is full of garbage because
		// of buffer wrapping performed by last task. In the latter case, we
		// should get rid of it. In the former case, we just discard 0 element.
	}

	int res = drake_task_autoexit();
	//debug(res);
	return res;
}

int
drake_kill()
{
	//debug("render_track");
	//drake_platform_time_get(killed[task->id - 1]);

	// Do nothing
	return 0;
}

int
drake_destroy()
{
	// Do nothing
	return 1;
}
