extern "C" {
#include "sort.h"
}
#include <parallel/algorithm>
#include <algorithm>
#include <vector>

#if 10
#define debug(var) printf("[%s:%s:%d] %s = \"%s\"\n", __FILE__, __FUNCTION__, __LINE__, #var, var); fflush(NULL)
#define debug_addr(var) printf("[%s:%s:%d] %s = \"%p\"\n", __FILE__, __FUNCTION__, __LINE__, #var, var); fflush(NULL)
#define debug_int(var) printf("[%s:%s:%d] %s = \"%d\"\n", __FILE__, __FUNCTION__, __LINE__, #var, var); fflush(NULL)
#define debug_size_t(var) printf("[%s:%s:%d] %s = \"%zu\"\n", __FILE__, __FUNCTION__, __LINE__, #var, var); fflush(NULL)
#else
#define debug(var)
#define debug_addr(var)
#define debug_int(var)
#define debug_size_t(var)
#endif

static int
greater(const void *a, const void *b)
{
	return *(int*)a - *(int*)b;
}

// A C++ container class
template<typename T>
class DynArray
{
	typedef T& reference;
	typedef const T& const_reference;
	typedef T* iterator;
	typedef const T* const_iterator;
	typedef ptrdiff_t difference_type;
	typedef size_t size_type;

	public:
	DynArray(T* buffer, size_t size)
	{
		this->buffer = buffer;
		this->size = size;
	}

	iterator begin()
	{
		return buffer;
	}

	iterator end()
	{
		return buffer + size;
	}

	protected:
		T* buffer;
		size_t size;
};

void sort(int* array, size_t size)
{ 
	DynArray<int> cppArray(array, size);
	//__gnu_parallel::sort(cppArray.begin(), cppArray.end(), __gnu_parallel::sequential_tag());
	std::sort(cppArray.begin(), cppArray.end());
	//std::stable_sort(cppArray.begin(), cppArray.end());
}

void merge(int** first1_ptr, int* last1, int** first2_ptr, int* last2, int** res_ptr, int* lastres)
{
	int *first1 = *first1_ptr;
	int *first2 = *first2_ptr;
	int *res = *res_ptr;

	// Loop inspired from std::merge in gcc-4.9
	while (first1 != last1 && first2 != last2 && res != lastres)
	{
		*res++ = (*first2 < *first1)? *first2++ : *first1++;
	}

	*first1_ptr = first1;
	*first2_ptr = first2;
	*res_ptr = res;
}
