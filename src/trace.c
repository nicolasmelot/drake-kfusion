/*
 Copyright 2015 Nicolas Melot

 This file is part of Drake-merge.

 Drake-merge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Drake-merge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Drake-merge. If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <drake.h>
#include <drake/link.h>
#include <string.h>

#include <drake-kfusion.h> 

#if 10
#define debug(var) printf("[%s:%s:%d:P%zu][%s] %s = \"%s\"\n", __FILE__, __FUNCTION__, __LINE__, drake_platform_core_id(), task->name, #var, var); fflush(NULL)
#define debug_addr(var) printf("[%s:%s:%d:P%zu][%s] %s = \"%p\"\n", __FILE__, __FUNCTION__, __LINE__, drake_platform_core_id(), task->name, #var, var); fflush(NULL)
#define debug_float(var) printf("[%s:%s:%d:P%zu][%s] %s = \"%.10f\"\n", __FILE__, __FUNCTION__, __LINE__, drake_platform_core_id(), task->name, #var, var); fflush(NULL)
#define debug_int(var) printf("[%s:%s:%d:P%zu][%s] %s = \"%d\"\n", __FILE__, __FUNCTION__, __LINE__, drake_platform_core_id(), task->name, #var, var); fflush(NULL)
#define debug_size_t(var) printf("[%s:%s:%d:P%zu][%s] %s = \"%zu\"\n", __FILE__, __FUNCTION__, __LINE__, drake_platform_core_id(), task->name, #var, var); fflush(NULL)
#else
#define debug(var)
#define debug_addr(var)
#define debug_int(var)
#define debug_size_t(var)
#endif

static drake_kfusion_t *init;
static size_t achieved;
FILE *cloud;

static
FILE*
new_frame_file(unsigned int index)
{
#define FILENAME_BASE "/tmp/drake-kfusion_frame_"
#define FILENAME_EXT ".vtk"
	int index_length = index == 0 ? 1 : (int)(floor(log(index))) + 1;
	char* filename = malloc(sizeof(char) * (strlen(FILENAME_BASE) + index_length + strlen(FILENAME_EXT) + 1));
	sprintf(filename, "%s%u%s", FILENAME_BASE, index, FILENAME_EXT);
	FILE* ff = fopen(filename, "w");

	if(ff != NULL)
	{
		fprintf(ff, "DATASET POLYDATA\nPOINTS \%zu float", init->compSize_x * init->compSize_y);
	}
	else
	{
		perror(filename);
	}
	free(filename);

	return ff;
}

int
drake_init(task_t *task, void* aux)
{
	init = (drake_kfusion_t*)aux;
	return 1;
}

int
drake_start(task_t *task)
{
	achieved = 0;
	return 1;
}

int
drake_run(task_t *task)
{
	static size_t point_counter = 0, frame_counter = 0, coord_counter = 0;
	static FILE* ff = NULL;
	// Fetch links
	link_tp input_link = pelib_map_read(string, link_tp)(pelib_map_find(string, link_tp)(task->pred, "input")).value;

	// Fetch buffer addresses
	size_t input_size = 0;
	float* input = (float*)pelib_cfifo_peekaddr(int)(input_link->buffer, 0, &input_size, NULL);

	size_t i;
	size_t instance_achieved = 0;
	for(i = 0; i < input_size; i++)
	{
		if(input[i] != FLT_MAX)
		{
			init->out[achieved + i] = input[i];
			instance_achieved++;

			if(point_counter == 0)
			{
				// create a new frame file and write point clouds
				ff = new_frame_file(frame_counter);
			}
			if(ff != NULL)
			{
				if(coord_counter == 0)
				{
					fprintf(ff, "\n");
				}
				coord_counter = (coord_counter + 1) % 3;

				// Append data to already opened file
				fprintf(ff, "%f ", input[i]);				
			}

			point_counter++;
			if(point_counter == init->compSize_x * init->compSize_y * 3)
			{
				point_counter = 0;
				frame_counter++;

				if(ff != NULL)
				{
					// Close file
					fclose(ff);
				}
			}
		}
		else
		{
			break;
		}
	}
	achieved += instance_achieved;

	pelib_cfifo_discard(int)(input_link->buffer, input_size);

	return drake_task_depleted(task);
}

int
drake_kill(task_t *task)
{
	//drake_platform_time_get(killed[task->id - 1]);

	// Do nothing
	return 0;
}

int
drake_destroy(task_t *task)
{
	// Do nothing
	return 1;
}
