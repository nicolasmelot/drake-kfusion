/*
 Copyright 2015 Nicolas Melot

 This file is part of Drake-merge.

 Drake-merge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Drake-merge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Drake-merge. If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <drake.h>
#include <drake/link.h>
#include <string.h>

#include <drake-kfusion.h> 
#include "kernels.h"
#include "util.h"

drake_declare_input(vertex, float);
drake_declare_input(normal, float);
drake_declare_input(refvertex, float);
drake_declare_input(refnormal, float);
drake_declare_input(raycast_pose, float);
drake_declare_input(pose, float);
drake_declare_input(break, int);
drake_declare_output(output, float);
drake_declare_output(pose, float);
drake_declare_output(render, float);
drake_declare_output(i, int);
drake_declare_output(j, int);

static drake_kfusion_t *args;
static int i = 0, j = 0;
static size_t frame = 0;
//static drake_time_t start, stop, duration;
//static link_tp vertex_link, normal_link, ref_vertex_link, ref_normal_link, raycast_pose_link, pose_link, output_pose_link, break_link, output_link, render_link, i_link;

int
drake_init(void* arg)
{
	/*
	vertex_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "vertex")).value;
	normal_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "normal")).value;
	ref_vertex_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "refvertex")).value;
	ref_normal_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "refnormal")).value;
	raycast_pose_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "raycast_pose")).value;
	pose_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "pose")).value;
	output_pose_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "pose")).value;
	break_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "break")).value;
	output_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "output")).value;
	render_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "render")).value;
	i_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "i")).value;
	*/

	args = (drake_kfusion_t*)arg;
/*
	start = drake_platform_time_alloc();
	stop = drake_platform_time_alloc();
	duration = drake_platform_time_alloc();
*/
	i = 0;
	j = 0;
	return 1;
}

int
drake_start()
{
	return 1;
}

#define PARALLEL_TRACK 1
#if PARALLEL_TRACK
struct track_args
{
	drake_kfusion_t *args;
	float *output, *vertex, *normal, *ref_vertex, *ref_normal, *pose;
	uint2 *inSize2, *compSize;
	Matrix4 *local_pose;
};

static int
parallel_track(void *aux)
{
	struct track_args *args = (struct track_args*)aux;
	trackKernel((TrackData*)args->output, (float3*)args->vertex, (float3*)args->normal, *args->inSize2, (float3*)args->ref_vertex, (float3*)args->ref_normal, *args->compSize, *(Matrix4*)args->pose, *args->local_pose, args->args->dist_threshold, args->args->normal_threshold, drake_task_core_id(), drake_task_width());

	return 1;
}
#endif

static float* last_output = 0;
static size_t last_size = 0;
int
drake_run()
{
	// Fetch links
	// Fetch buffer addresses
	size_t vertex_size = 0;
	//float* vertex = (float*)pelib_cfifo_peekaddr(int)(vertex_link->buffer, 0, &vertex_size, NULL);
	float *vertex = drake_input_buffer(vertex)(0, &vertex_size, NULL);
	size_t normal_size = 0;
	//float* normal = (float*)pelib_cfifo_peekaddr(int)(normal_link->buffer, 0, &normal_size, NULL);
	float *normal = drake_input_buffer(normal)(0, &normal_size, NULL);
	size_t ref_vertex_size = 0;
	//float* ref_vertex = (float*)pelib_cfifo_peekaddr(int)(ref_vertex_link->buffer, 0, &ref_vertex_size, NULL);
	float *ref_vertex = drake_input_buffer(refvertex)(0, &ref_vertex_size, NULL);
	size_t ref_normal_size = 0;
	//float* ref_normal = (float*)pelib_cfifo_peekaddr(int)(ref_normal_link->buffer, 0, &ref_normal_size, NULL);
	float *ref_normal = drake_input_buffer(refnormal)(0, &ref_normal_size, NULL);
	size_t raycast_pose_size = 0;
	//float* raycast_pose = (float*)pelib_cfifo_peekaddr(int)(raycast_pose_link->buffer, 0, &raycast_pose_size, NULL);
	float *raycast_pose_wrap;
	float *raycast_pose = drake_input_buffer(raycast_pose)(0, &raycast_pose_size, &raycast_pose_wrap);

	// Make sure we read the latest pose issued from raycast
	// If there is strictly more data than one pose, then discard first pose and update pose pointer
	if(raycast_pose_size > 16 || raycast_pose_wrap != NULL)
	{
		//debug(raycast_pose);
		//debug(raycast_pose_size);
		//debug(raycast_pose_wrap);
		// Discard pose
		//pelib_cfifo_discard(int)(raycast_pose_link->buffer, 16);
		//debug(16);
		drake_input_discard(raycast_pose)(16);
		// Update pointer
		//raycast_pose = (float*)pelib_cfifo_peekaddr(int)(raycast_pose_link->buffer, 0, &raycast_pose_size, NULL);
		raycast_pose = drake_input_buffer(raycast_pose)(0, &raycast_pose_size, NULL);
		// If the new reading pose address is smaller than a pose, then it's garbage and the pose is wrapped around
		// Then discard garbage, wrap fifo around and update pose reading address
		/*
		if(raycast_pose_size < 16)
		{
			// wrap around buffer
			//pelib_cfifo_discard(int)(raycast_pose_link->buffer, raycast_pose_size);
			drake_input_discard(raycast_pose)(raycast_pose_size);
			//raycast_pose = (float*)pelib_cfifo_peekaddr(int)(raycast_pose_link->buffer, 0, &raycast_pose_size, NULL);
			raycast_pose = drake_input_buffer(raycast_pose)(0, &raycast_pose_size, NULL);
		}
		*/
	}

	size_t pose_size = 0;
	//float* pose = (float*)pelib_cfifo_peekaddr(int)(pose_link->buffer, 0, &pose_size, NULL);
	float *pose = drake_input_buffer(pose)(0, &pose_size, NULL);
	size_t output_pose_size = 0;
	//float* output_pose = (float*)pelib_cfifo_writeaddr(int)(output_pose_link->buffer, &output_pose_size, NULL);
	float *output_pose = drake_output_buffer(pose)(&output_pose_size, NULL);
	size_t break_size = 0;
	//int* brk = pelib_cfifo_peekaddr(int)(break_link->buffer, 0, &break_size, NULL);
	break_size = drake_input_available_continuous(break);	
	//int *brk = drake_input_buffer(break)(0, &break_size, NULL);
	//size_t i_size = pelib_cfifo_capacity(int)(i_link->buffer) - pelib_cfifo_length(int)(i_link->buffer);
	size_t i_size;
	int *i_ptr = drake_output_buffer(i)(&i_size, NULL);
	//size_t j_size;
	//int *j_ptr = drake_output_buffer(j)(&j_size, NULL);
	//size_t j_size = pelib_cfifo_capacity(int)(j_link->buffer) - pelib_cfifo_length(int)(j_link->buffer);

	size_t output_size;
	//float *output = (float*)pelib_cfifo_writeaddr(int)(output_link->buffer, &output_size, NULL);
	float *output = drake_output_buffer(output)(&output_size, NULL);

	size_t render_size;
	//float *render = (float*)pelib_cfifo_writeaddr(int)(render_link->buffer, &render_size, NULL);
	float *render = drake_output_buffer(render)(&render_size, NULL);

	size_t inSize = 3 * args->compSize.x * args->compSize.y / pow(4, args->pyramid.size() - i - 1);
	size_t compSize = 3 * args->compSize.x * args->compSize.y;
	size_t outSize = inSize / 3 * 8;

#if DEBUG_INCONSISTENCIES
	if(
		/*
		pelib_cfifo_capacity(int)(vertex_link->buffer) < inSize ||
		pelib_cfifo_capacity(int)(normal_link->buffer) < inSize ||
		pelib_cfifo_capacity(int)(ref_vertex_link->buffer) < compSize ||
		pelib_cfifo_capacity(int)(ref_normal_link->buffer) < compSize ||
		pelib_cfifo_capacity(int)(pose_link->buffer) < 16 ||
		pelib_cfifo_capacity(int)(output_pose_link->buffer) < 16 ||
		pelib_cfifo_capacity(int)(break_link->buffer) == 0 ||
		pelib_cfifo_capacity(int)(output_link->buffer) < outSize ||
		pelib_cfifo_capacity(int)(render_link->buffer) < outSize ||
		pelib_cfifo_capacity(int)(raycast_pose_link->buffer) < 16 * 2 // 2 poses
		*/
		   drake_input_capacity(vertex) < inSize
		|| drake_input_capacity(normal) < inSize
		|| drake_input_capacity(refvertex) < compSize
		|| drake_input_capacity(refnormal) < compSize
		|| drake_input_capacity(pose) < 16
		|| drake_input_capacity(break) < 1
		|| drake_input_capacity(raycast_pose) < 16 * 2
		|| drake_output_capacity(pose) < 16
		|| drake_output_capacity(output) < outSize
		|| drake_output_capacity(render) < outSize
	)
	{
		//debug("Track");
		const char* buffer_str;
		size_t buffer_size, required_size;
		//if(inSize > pelib_cfifo_capacity(int)(vertex_link->buffer))
		if(inSize > drake_input_capacity(vertex))
		{
			required_size = inSize;
			buffer_str = "vertex";
			//buffer_size = pelib_cfifo_capacity(int)(vertex_link->buffer);
			buffer_size = drake_input_capacity(vertex);
		}
		//else if(inSize > pelib_cfifo_capacity(int)(normal_link->buffer))
		else if(inSize > drake_input_capacity(normal))
		{
			required_size = inSize;
			buffer_str = "normal";
			//buffer_size = pelib_cfifo_capacity(int)(normal_link->buffer);
			buffer_size = drake_input_capacity(normal);
		}
		//else if(inSize > pelib_cfifo_capacity(int)(ref_vertex_link->buffer))
		else if(inSize > drake_input_capacity(refvertex))
		{
			required_size = compSize;
			buffer_str = "refvertex";
			//buffer_size = pelib_cfifo_capacity(int)(ref_vertex_link->buffer);
			buffer_size = drake_input_capacity(refvertex);
		}
		//else if(inSize > pelib_cfifo_capacity(int)(ref_normal_link->buffer))
		else if(inSize > drake_input_capacity(refnormal))
		{
			required_size = compSize;
			buffer_str = "refnormal";
			//buffer_size = pelib_cfifo_capacity(int)(ref_normal_link->buffer);
			buffer_size = drake_input_capacity(refnormal);
		}
		//else if(inSize > pelib_cfifo_capacity(int)(pose_link->buffer))
		else if(16 > drake_input_capacity(pose))
		{
			required_size = 16;
			buffer_str = "pose";
			//buffer_size = pelib_cfifo_capacity(int)(pose_link->buffer);
			buffer_size = drake_input_capacity(pose);
		}
		//else if(pelib_cfifo_capacity(int)(break_link->buffer) == 0)
		else if(1 > drake_input_capacity(break))
		{
			required_size = 1;
			buffer_str = "break";
			//buffer_size = pelib_cfifo_capacity(int)(break_link->buffer);	
			buffer_size = drake_input_capacity(break);
		}
		//else if(pelib_cfifo_capacity(int)(raycast_pose_link->buffer) < 16 * sizeof(float) * 2)
		else if(16 * 2 > drake_input_capacity(raycast_pose))
		{
			required_size = 16 * 2;
			buffer_str = "raycast pose";
			//buffer_size = pelib_cfifo_capacity(int)(raycast_pose_link->buffer);
			buffer_size = drake_input_capacity(raycast_pose);
		}
		//else if(pelib_cfifo_capacity(int)(output_link->buffer) == 0)
		else if(outSize > drake_output_capacity(output))
		{
			required_size = outSize;
			buffer_str = "output";
			//buffer_size = pelib_cfifo_capacity(int)(output_link->buffer);
			buffer_size = drake_output_capacity(output);
		}
		//else if(pelib_cfifo_capacity(int)(render_link->buffer) == 0)
		else if(outSize > drake_output_capacity(render))
		{
			required_size = outSize;
			buffer_str = "render";
			//buffer_size = pelib_cfifo_capacity(int)(render_link->buffer);
			buffer_size = drake_output_capacity(render);
		}
		//else if(inSize > pelib_cfifo_capacity(int)(output_pose_link->buffer))
		else if(16 > drake_output_capacity(pose))
		{
			required_size = 16;
			buffer_str = "output_pose";
			//buffer_size = pelib_cfifo_capacity(int)(output_pose_link->buffer);
			buffer_size = drake_output_capacity(pose);
		}
		printf("[%s:%s:%d] %s buffer cannot fit input or output frame (%zu > %zu)\n", __FILE__, __FUNCTION__, __LINE__, buffer_str, required_size, buffer_size);
		abort();
	}
#endif

	// Manage breaks received from update_pose
	if(break_size > 0) // && brk[0] == 0)
	{
		//debug("break!");
		if(i == (int)args->pyramid.size() - 1)
		{
			if(render_size > outSize)
			{
				//pelib_cfifo_fill(int)(render_link->buffer, render_size);
				drake_output_commit(render)(render_size - outSize);
				//render = (float*)pelib_cfifo_writeaddr(int)(render_link->buffer, &render_size, NULL);
				render = drake_output_buffer(render)(&render_size, NULL);
			}
			if(render_size == outSize)
			{
				memcpy(render, output, sizeof(float) * outSize);
				drake_output_commit(render)(outSize);
			}
		}
		//pelib_cfifo_fill(int)(render_link->buffer, outSize);
		//drake_output_commit(render)(outSize);
		// If reduce reported that no further computation is needed, then just discard all input frames
		// until we can proceed with the next, higher resolution input frame
		//pelib_cfifo_discard(int)(vertex_link->buffer, inSize);
		drake_input_discard(vertex)(inSize);
		//pelib_cfifo_discard(int)(normal_link->buffer, inSize);
		drake_input_discard(normal)(inSize);
		j = 0;
		i = (i + 1) % args->pyramid.size();
		//pelib_cfifo_discard(int)(break_link->buffer, 1);
		drake_input_discard(break)(1);
		if(j == 0 && i == 0)
		{
			frame++;
			if(frame > 3)
			{
				//pelib_cfifo_discard(int)(ref_vertex_link->buffer, args->compSize.x * args->compSize.y * 3);
				drake_input_discard(refvertex)(args->compSize.x * args->compSize.y * 3);
				//pelib_cfifo_discard(int)(ref_normal_link->buffer, args->compSize.x * args->compSize.y * 3);
				drake_input_discard(refnormal)(args->compSize.x * args->compSize.y * 3);

				ref_vertex = drake_input_buffer(refvertex)(0, &ref_vertex_size, NULL);
				ref_normal = drake_input_buffer(refnormal)(0, &ref_normal_size, NULL);
			}
		}

		// And update input and output buffers and size available
		vertex = drake_input_buffer(vertex)(0, &vertex_size, NULL);
		normal = drake_input_buffer(normal)(0, &normal_size, NULL);
		//brk = drake_input_buffer(break)(0, &break_size, NULL);
		break_size = drake_input_available_continuous(break);
		render = drake_output_buffer(render)(&render_size, NULL);

		// And update values of reference
		inSize = 3 * args->compSize.x * args->compSize.y / pow(4, args->pyramid.size() - i - 1);
		compSize = 3 * args->compSize.x * args->compSize.y;
		outSize = inSize / 3 * 8;

		/*
		if(vertex_size < inSize && vertex_size > 0)
		{
			//pelib_cfifo_discard(int)(vertex_link->buffer, vertex_size);
			debug(vertex_size);
			drake_input_discard(vertex)(vertex_size);
			vertex = drake_input_buffer(vertex)(0, &vertex_size, NULL);
		}
		if(normal_size < inSize && normal_size > 0)
		{
			//pelib_cfifo_discard(int)(normal_link->buffer, normal_size);
			debug(normal_size);
			drake_input_discard(normal)(normal_size);
			normal = drake_input_buffer(normal)(0, &normal_size, NULL);
		}
		if(ref_vertex_size < compSize)
		{
			//pelib_cfifo_discard(int)(ref_vertex_link->buffer, ref_vertex_size);
			debug(ref_vertex_size);
			drake_input_discard(refvertex)(ref_vertex_size);
			ref_vertex = drake_input_buffer(refvertex)(0, &ref_vertex_size, NULL);
		}
		if(ref_normal_size < compSize)
		{
			//pelib_cfifo_discard(int)(ref_normal_link->buffer, ref_normal_size);
			debug(ref_normal_size);
			drake_input_discard(refnormal)(ref_normal_size);
			ref_normal = drake_input_buffer(refnormal)(0, &ref_normal_size, NULL);
		}
		if(pose_size < 16)
		{
			//pelib_cfifo_discard(int)(pose_link->buffer, pose_size);
			debug(pose_size);
			drake_input_discard(pose)(pose_size);
			pose = drake_input_buffer(pose)(0, &pose_size, NULL);
		}
		*/
	}

	if(vertex_size > inSize)
	{
		//pelib_cfifo_discard(int)(vertex_link->buffer, vertex_size);
		//debug(vertex_size);
		//debug(vertex_size - inSize);
		drake_input_discard(vertex)(vertex_size - inSize);
		vertex = drake_input_buffer(vertex)(0, &vertex_size, NULL);
	}
	if(normal_size > inSize)
	{
		//pelib_cfifo_discard(int)(normal_link->buffer, normal_size);
		//debug(normal_size);
		drake_input_discard(normal)(normal_size - inSize);
		normal = drake_input_buffer(normal)(0, &normal_size, NULL);
	}
	if(ref_vertex_size > compSize)
	{
		//pelib_cfifo_discard(int)(ref_vertex_link->buffer, ref_vertex_size);
		//debug(ref_vertex_size);
		drake_input_discard(refvertex)(ref_vertex_size - compSize);
		ref_vertex = drake_input_buffer(refvertex)(0, &ref_vertex_size, NULL);
	}
	if(ref_normal_size > compSize)
	{
		//pelib_cfifo_discard(int)(ref_normal_link->buffer, ref_normal_size);
		//debug(ref_normal_size);
		drake_input_discard(refnormal)(ref_normal_size - compSize);
		ref_normal = drake_input_buffer(refnormal)(0, &ref_normal_size, NULL);
	}
	if(pose_size > 16)
	{
		//pelib_cfifo_discard(int)(pose_link->buffer, pose_size);
		//debug(pose_size);
		drake_input_discard(pose)(pose_size - 16);
		pose = drake_input_buffer(pose)(0, &pose_size, NULL);
	}

	//float *output_wrap;
	//output = (float*)pelib_cfifo_writeaddr(int)(output_link->buffer, &output_size, &output_wrap);
	//output = drake_output_buffer(output)(&output_size, &output_wrap);
	//int push_mock_index = 0;

	//float *render_wrap;
	//render = (float*)pelib_cfifo_writeaddr(int)(render_link->buffer, &render_size, &render_wrap);
	//render = drake_output_buffer(render)(&render_size, &render_wrap);

	//float *output_pose_wrap;
	//output_pose = (float*)pelib_cfifo_writeaddr(int)(output_pose_link->buffer, &output_pose_size, &output_pose_wrap);
	//output_pose = drake_output_buffer(pose)(&output_pose_size, &output_pose_wrap);

	/*
	if(push_mock_index != 0)
	{
		*i_ptr = i;
		//pelib_cfifo_push(int)(i_link->buffer, i);
		debug(1);
		drake_output_commit(i)(1);
	}
	*/
#define DEBUG_GUARD 0
#if DEBUG_GUARD
debug("######### Track ###########");
debug(i);
debug(j);
debug(inSize);
debug(outSize);
debug(ref_vertex);
debug(vertex_size);
debug(normal_size);
debug(ref_vertex_size);
debug(ref_normal_size);
debug(raycast_pose_size);
debug(pose_size);
debug(output_pose_size);
debug(output_size);
debug(render_size);
debug(break_size);
//debug((break_size > 0 ? *brk : - 1));
debug(i_size);
debug(drake_task_instance());
debug("########## End ############");
#endif
	if(
		   vertex_size >= inSize
		&& normal_size >= inSize
		&& ref_vertex_size >= inSize
		&& ref_normal_size >= inSize
		&& raycast_pose_size >= 16
		&& pose_size >= 16
		&& output_pose_size >= 16
		&& output_size >= outSize
		//&& render_size >= outSize
		//&& (break_size >= 1)
		//&& *brk != 0
		&& i_size >= 1
		//&& j_size >= 1
	)
	{
		// Break token received from reduce task says we should compute more

		// Now do normal work
		{
			Matrix4 reference_pose;
			if(frame <= 3)
			{
				reference_pose = args->pose;
			}
			else
			{
				reference_pose = *(Matrix4*)pose;
			}
			Matrix4 local_pose = getCameraMatrix(args->k) * inverse(*(Matrix4*)raycast_pose);

			if((i == 0 && j == 0))
			{
				last_output = output;
			}
			last_size = outSize;

			size_t inSize_x = args->compSize.x / pow(2, args->pyramid.size() - i - 1);
			size_t inSize_y = args->compSize.y / pow(2, args->pyramid.size() - i - 1);

			uint2 inSize2 = make_uint2(inSize_x, inSize_y);
			uint2 compSize = make_uint2(args->compSize.x, args->compSize.y);

	if(output_size > outSize)
	{
		//pelib_cfifo_fill(int)(output_link->buffer, output_size);
		drake_output_commit(output)(output_size - outSize);
		//output = (float*)pelib_cfifo_writeaddr(int)(output_link->buffer, &output_size, NULL);
		//debug(output_size);
		output = drake_output_buffer(output)(&output_size, NULL);
		//push_mock_index = 1;
	}
	if(output_pose_size > 16)
	{
		//pelib_cfifo_fill(int)(output_pose_link->buffer, output_pose_size);
		//debug(output_pose_size);
		drake_output_commit(pose)(output_pose_size - 16);
		output_pose = drake_output_buffer(pose)(&output_pose_size, NULL);
		//push_mock_index = 1;
	}
//	drake_platform_time_get(start);
			//debug("Track");
#if PARALLEL_TRACK
#warning Parallel track
	struct track_args pargs;
	pargs.output = output;
	pargs.vertex = vertex;
	pargs.normal = normal;
	pargs.inSize2 = &inSize2;
	pargs.ref_vertex = ref_vertex;
	pargs.ref_normal = ref_normal;
	pargs.compSize = &compSize;
	pargs.pose = pose;
	pargs.local_pose = &local_pose;
	pargs.args = args;

	drake_task_pool_run(parallel_track, &pargs);
#else
#warning Sequential track
			trackKernel((TrackData*)output, (float3*)vertex, (float3*)normal, inSize2, (float3*)ref_vertex, (float3*)ref_normal, compSize, *(Matrix4*)pose, local_pose, args->dist_threshold, args->normal_threshold, 0, 1);
#endif
//	drake_platform_time_get(stop);

			// make sure we can actually copy in such a trivial manner
			static_assert(std::is_trivially_copyable<TrackData>::value, "TrackData is not trivially copyable");

			//pelib_cfifo_fill(int)(output_link->buffer, outSize);
			drake_output_commit(output)(outSize);
			// Send loop indices to subsequent tasks
			//pelib_cfifo_push(int)(i_link->buffer, i);
			*i_ptr = i;
			drake_output_commit(i)(1);
			//*j_ptr = j;
			//drake_output_commit(j)(1);
			//pelib_cfifo_push(int)(j_link->buffer, j);
			// Send the new pose to update pose
			memcpy(output_pose, &reference_pose, sizeof(Matrix4));

			//pelib_cfifo_fill(int)(output_pose_link->buffer, 16);
			drake_output_commit(pose)(16);
			//pelib_cfifo_discard(int)(break_link->buffer, 1);
			//drake_input_discard(break)(1);

			j++;
			// TODO: superfluous with break management above?
			if(j == args->pyramid[args->pyramid.size() - i - 1])
			{
				i = (i + 1) % args->pyramid.size();
				j = 0;

				// Take next, higher resolution input frame
				//pelib_cfifo_discard(int)(vertex_link->buffer, inSize);
				drake_input_discard(vertex)(inSize);
				//pelib_cfifo_discard(int)(normal_link->buffer, inSize);
				drake_input_discard(normal)(inSize);

				if(j == 0 && i == 0)
				{
					//pelib_cfifo_fill(int)(render_link->buffer, outSize);
					//debug("Render");
					//debug(outSize);
					if(render_size > outSize)
					{
						//pelib_cfifo_fill(int)(render_link->buffer, render_size);
						drake_output_commit(render)(render_size - outSize);
						//render = (float*)pelib_cfifo_writeaddr(int)(render_link->buffer, &render_size, NULL);
						render = drake_output_buffer(render)(&render_size, NULL);
					}
					if(render_size == outSize)
					{
							memcpy(render, output, sizeof(float) * outSize);
									drake_output_commit(render)(outSize);
					}
					frame++;
					inSize = 3 * args->compSize.x * args->compSize.y / pow(4, args->pyramid.size() - i - 1);
					outSize = inSize / 3 * 8;

					// Discard reference inputs
					if(frame > 3)
					{
						//pelib_cfifo_discard(int)(ref_vertex_link->buffer, args->compSize.x * args->compSize.y * 3);
						drake_input_discard(refvertex)(args->compSize.x * args->compSize.y * 3);
						//pelib_cfifo_discard(int)(ref_normal_link->buffer, args->compSize.x * args->compSize.y * 3);
						drake_input_discard(refnormal)(args->compSize.x * args->compSize.y * 3);
					}
				}
			}
			//pelib_cfifo_discard(int)(pose_link->buffer, 16);
			drake_input_discard(pose)(16);
		}
//	drake_platform_time_subtract(duration, stop, start);
//	printf("track: ");
//	drake_platform_time_printf(stdout, duration);
//	printf("\n");
	}
	//else

	// We conservatively assume the next iteration will involve a bigger input token
	inSize = 3 * args->compSize.x * args->compSize.y / pow(4, args->pyramid.size() - i - 1);
	outSize = inSize / 3 * 8;

	{
		// Either input channel is empty, either it is full of garbage because
		// of buffer wrapping performed by last task. In the latter case, we
		// should get rid of it. In the former case, we just discard 0 element.
	}

	// Compute size requited for next round
	// Wrap buffer around if remainder is too small

	//return drake_task_depleted(normal_link->prod) && pelib_cfifo_length(int)(normal_link->buffer) == 0 &&
	//	drake_task_depleted(vertex_link->prod) && pelib_cfifo_length(int)(vertex_link->buffer) == 0;
	int res = drake_input_depleted(normal) && drake_input_depleted(vertex);
	//debug(res);
	return res;
}

int
drake_kill()
{
	//debug("track");
	//drake_platform_time_get(killed[task->id - 1]);

	// Do nothing
	return 0;
}

int
drake_destroy()
{
	// Do nothing
	return 1;
}

