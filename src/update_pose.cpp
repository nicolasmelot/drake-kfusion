/*
 Copyright 2015 Nicolas Melot

 This file is part of Drake-merge.

 Drake-merge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Drake-merge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Drake-merge. If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <drake.h>
#include <drake/link.h>
#include <string.h>

#include <drake-kfusion.h> 
#include "kernels.h"
#include "util.h"

drake_declare_input(reduction, float);
drake_declare_input(pose, float);
drake_declare_output(integrate, float);
drake_declare_output(intecast, float);
drake_declare_output(track, float);
drake_declare_output(break, int);

static drake_kfusion_t *args;
static int j = 0;
static unsigned i = 0;
//static drake_time_t start, stop, duration;
//static link_tp reduction_link, integrate_link, input_pose_link, intecast_pose_link, track_pose_link, break_link;

int
drake_init(void* arg)
{
	/*
	reduction_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "reduction")).value;
	integrate_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "integrate")).value;
	input_pose_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "pose")).value;
	intecast_pose_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "intecast")).value;
	track_pose_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "track")).value;
	break_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "break")).value;
	*/
	args = (drake_kfusion_t*)arg;
/*
	start = drake_platform_time_alloc();
	stop = drake_platform_time_alloc();
	duration = drake_platform_time_alloc();
*/
	i = 0;
	j = 0;
	return 1;
}

int
drake_start()
{
	size_t pose_size = 0;
	size_t poseSize = 16;
	//float* pose = (float*)pelib_cfifo_writeaddr(int)(track_pose_link->buffer, &pose_size, NULL);
	float *pose = drake_output_buffer(track)(&pose_size, NULL);
	if(pose_size > poseSize)
	{
		//pelib_cfifo_fill(int)(track_pose_link->buffer, track_pose_size);
		drake_output_commit(track)(pose_size - poseSize);
		pose = drake_output_buffer(track)(&pose_size, NULL);
	}

	memcpy(pose, &args->pose, sizeof(Matrix4));
	//pelib_cfifo_fill(int)(track_pose_link->buffer, 16);
	drake_output_commit(track)(poseSize);
	
	//pelib_cfifo_push(int)(break_link->buffer, 1);
	/*
	size_t break_size;
	int *brk = drake_output_buffer(break)(&break_size, NULL);
	*brk = 1;
	drake_output_commit(break)(1);
	*/
	return 1;
}

int
drake_run()
{
	// Fetch buffer addresses
	size_t reduction_size = 0;
	//float* reduction = (float*)pelib_cfifo_peekaddr(int)(reduction_link->buffer, 0, &reduction_size, NULL);
	float *reduction = drake_input_buffer(reduction)(0, &reduction_size, NULL);
	size_t integrate_size;
	//float *integrate = (float*)pelib_cfifo_writeaddr(int)(integrate_link->buffer, &integrate_size, NULL);
	float *integrate = drake_output_buffer(integrate)(&integrate_size, NULL);
	size_t input_pose_size;
	//float *input_pose = (float*)pelib_cfifo_peekaddr(int)(input_pose_link->buffer, 0, &input_pose_size, NULL);
	float *input_pose = drake_input_buffer(pose)(0, &input_pose_size, NULL);
	size_t intecast_pose_size;
	//float *intecast_pose = (float*)pelib_cfifo_writeaddr(int)(intecast_pose_link->buffer, &intecast_pose_size, NULL);
	float *intecast_pose = drake_output_buffer(intecast)(&intecast_pose_size, NULL);
	size_t track_pose_size;
	//float *track_pose = (float*)pelib_cfifo_writeaddr(int)(track_pose_link->buffer, &track_pose_size, NULL);
	float *track_pose = drake_output_buffer(track)(&track_pose_size, NULL);
	size_t break_size;
	//int *brk = drake_output_buffer(break)(&break_size, NULL);
	break_size = drake_output_available_continuous(break);

	size_t inSize = 8 * 32;
	size_t poseSize = 16; // Size of 4x4 pose matrix

#if DEBUG_INCONSISTENCIES
	if(
		/*
		   pelib_cfifo_capacity(int)(reduction_link->buffer) < inSize
		|| pelib_cfifo_capacity(int)(input_pose_link->buffer) < poseSize
		|| pelib_cfifo_capacity(int)(intecast_pose_link->buffer) < poseSize
		|| pelib_cfifo_capacity(int)(track_pose_link->buffer) < poseSize
		|| pelib_cfifo_capacity(int)(integrate_link->buffer) < inSize
		|| pelib_cfifo_capacity(int)(break_link->buffer) < 1
		*/
		   drake_input_capacity(reduction) < inSize
		|| drake_input_capacity(pose) < poseSize
		|| drake_output_capacity(intecast) < poseSize
		|| drake_output_capacity(track) < poseSize
		|| drake_output_capacity(integrate) < inSize
		|| drake_output_capacity(break) < 1
	)
	{
		const char *buffer;
		size_t buffer_expected;
		size_t buffer_actual;
		//if(pelib_cfifo_capacity(int)(reduction_link->buffer) < inSize)
		if(drake_input_capacity(reduction) < inSize)
		{
			buffer = "reduction";
			buffer_expected = inSize;
			//buffer_actual = pelib_cfifo_capacity(int)(reduction_link->buffer);
			buffer_actual = drake_input_capacity(reduction);
		}
		//else if(pelib_cfifo_capacity(int)(input_pose_link->buffer) < poseSize)
		if(drake_input_capacity(pose) < poseSize)
		{
			buffer = "input_pose";
			buffer_expected = 16;
			//buffer_actual = pelib_cfifo_capacity(int)(input_pose_link->buffer);
			buffer_actual = drake_input_capacity(pose);
		}
		//else if(pelib_cfifo_capacity(int)(intecast_pose_link->buffer) < poseSize)
		if(drake_output_capacity(intecast) < poseSize)
		{
			buffer = "intecast_pose";
			buffer_expected = 16;
			//buffer_actual = pelib_cfifo_capacity(int)(intecast_pose_link->buffer);
			buffer_actual = drake_output_capacity(intecast);
		}
		//else if(pelib_cfifo_capacity(int)(track_pose_link->buffer) < poseSize)
		if(drake_output_capacity(track) < poseSize)
		{
			buffer = "track_pose";
			buffer_expected = 16;
			//buffer_actual = pelib_cfifo_capacity(int)(track_pose_link->buffer);
			buffer_actual = drake_output_capacity(track);
		}
		//else if(pelib_cfifo_capacity(int)(integrate_link->buffer) < inSize)
		if(drake_output_capacity(integrate) < inSize)
		{
			buffer = "integrate";
			buffer_expected = inSize;
			//buffer_actual = pelib_cfifo_capacity(int)(integrate_link->buffer);
			buffer_actual = drake_output_capacity(integrate);
		}
		//else if(pelib_cfifo_capacity(int)(break_link->buffer) < 1)
		if(drake_output_capacity(break) < 1)
		{
			buffer = "break";
			buffer_expected = 1;
			//buffer_actual = pelib_cfifo_capacity(int)(break_link->buffer);
			buffer_actual = drake_output_capacity(break);
		}

		printf("[%s:%s:%d] %s buffer cannot fit input or output frame (%zu > %zu)\n", __FILE__, __FUNCTION__, __LINE__, buffer, buffer_expected, buffer_actual);
		abort();
	}
#endif

	if(reduction_size > inSize)
	{
		//pelib_cfifo_discard(int)(reduction_link->buffer, reduction_size);
		drake_input_discard(reduction)(reduction_size - inSize);
		reduction = drake_input_buffer(reduction)(0, &reduction_size, NULL);
	}
	if(input_pose_size > poseSize)
	{
		//pelib_cfifo_discard(int)(input_pose_link->buffer, input_pose_size);
		drake_input_discard(pose)(input_pose_size - poseSize);
		input_pose = drake_input_buffer(pose)(0, &input_pose_size, NULL);
	}

	// Wrap buffer around if remainder memory is too small
	//float *intecast_pose_wrap = NULL;
	//intecast_pose = (float*)pelib_cfifo_writeaddr(int)(intecast_pose_link->buffer, &intecast_pose_size, NULL);
	//float *track_pose_wrap = NULL;
	//track_pose = (float*)pelib_cfifo_writeaddr(int)(track_pose_link->buffer, &track_pose_size, NULL);

	//float *integrate_wrap = NULL;
	//integrate = (float*)pelib_cfifo_writeaddr(int)(integrate_link->buffer, &integrate_size, &integrate_wrap);
#define DEBUG_GUARD 0
#if DEBUG_GUARD
debug("###### Update Pose ########");
debug(inSize);
debug(poseSize);
debug(reduction_size);
debug(input_pose_size);
debug(intecast_pose_size);
debug(track_pose_size);
debug(track_pose_size);
debug(integrate_size);
debug(break_size);
debug("########## End ############");
#endif
	if(
		   reduction_size >= inSize
		&& input_pose_size >= poseSize
		&& intecast_pose_size >= poseSize
		&& track_pose_size >= poseSize
		&& integrate_size >= inSize
		&& break_size >= 1
	)
	{
//	drake_platform_time_get(start);
		bool new_pose_ok = updatePoseKernel(*(Matrix4*)input_pose, reduction, args->icp_threshold);
//	drake_platform_time_get(stop);
	if(track_pose_size > poseSize)
	{
		//pelib_cfifo_fill(int)(track_pose_link->buffer, track_pose_size);
		drake_output_commit(track)(track_pose_size - poseSize);
		track_pose = drake_output_buffer(track)(&track_pose_size, NULL);
	}
		memcpy(track_pose, input_pose, 16 * sizeof(float));

		j++;
		if((new_pose_ok || j == args->pyramid[args->pyramid.size() - i - 1]) && i == args->pyramid.size() - 1)
		{
			// Send data to intecast if last i and j iteration, whether the pose is OK
			// or we reached max j iteration

			if(i == args->pyramid.size() - 1) // Tautology?
			{
				// And forward reduction to integrate & raycast
				//pelib_cfifo_fill(int)(integrate_link->buffer, inSize);
	if(integrate_size > inSize)
	{
		//pelib_cfifo_fill(int)(integrate_link->buffer, integrate_size);
		drake_output_commit(integrate)(integrate_size - inSize);
		integrate = drake_output_buffer(integrate)(&integrate_size, NULL);
	}
				memcpy(integrate, reduction, inSize * sizeof(float));
				drake_output_commit(integrate)(inSize);
				//pelib_cfifo_fill(int)(intecast_pose_link->buffer, 16);
				//debug(16);
	if(intecast_pose_size > poseSize)
	{
		//pelib_cfifo_fill(int)(intecast_pose_link->buffer, intecast_pose_size);
		drake_output_commit(intecast)(intecast_pose_size - poseSize);
		intecast_pose = drake_output_buffer(intecast)(&intecast_pose_size, NULL);
	}
				memcpy(intecast_pose, track_pose, sizeof(float) * poseSize);
				drake_output_commit(intecast)(16);
			}
		}
		//pelib_cfifo_fill(int)(track_pose_link->buffer, 16);
		drake_output_commit(track)(poseSize);
		//pelib_cfifo_discard(int)(input_pose_link->buffer, 16);
		drake_input_discard(pose)(16);
			

		// If that was the last iteration for a frame refinement, (j == iteration[i] - 1) then we always should request for more computation.
		if(new_pose_ok && j < args->pyramid[args->pyramid.size() - i - 1])
		{
			// Either the pose is OK, either we are at the last j iteration.
			// Either way, increment i (and possibly wrap around), reset j
			// and send break signal so track increments (and possibly wraps)
			// i.

			// If last i and j iteration (because pose is OK or because last j iteration)
			// Then tell what should be next task to run
			if(i == args->pyramid.size() - 1)
			{
			}
			else
			{
			}
			j = 0;
			i = (i + 1) % args->pyramid.size();
			//pelib_cfifo_push(int)(break_link->buffer, 1);
			drake_output_commit(break)(1);
		}
		else
		{
			if(j == args->pyramid[args->pyramid.size() - i - 1] && i == args->pyramid.size())
			{
			}
			else
			{
			}
			if(j == args->pyramid[args->pyramid.size() - i - 1])
			{
				j = 0;
				i = (i + 1) % args->pyramid.size();
			}
			// Neither OK new pose or last j iteration
			// j is already incremented, there is nothing to do
#if 0
			if(!new_pose_ok) // Only push 1 if more computation is actually needed for this frame
			{
				//pelib_cfifo_push(int)(break_link->buffer, 1);
				*brk = 1;
				drake_output_commit(break)(1);
				debug("Next is track");
			}
			else
			{
				// We break this iteration
				brk[0] = 0;
				//pelib_cfifo_push(int)(break_link->buffer, 0);
				drake_output_commit(break)(1);
				brk = drake_output_buffer(break)(&break_size, NULL);
				// but we allow the next one
				brk[0] = 1;
				//pelib_cfifo_push(int)(break_link->buffer, 1);
				drake_output_commit(break)(1);
				brk = drake_output_buffer(break)(&break_size, NULL);

				j = 0;
				//debug(i);
				i = (i + 1) % args->pyramid.size();
				debug("Next is intecast");
			}
#endif
		}

		// In any way, always discard input
		//pelib_cfifo_discard(int)(reduction_link->buffer, inSize);
		drake_input_discard(reduction)(inSize);
/*
	drake_platform_time_subtract(duration, stop, start);
	printf("update pose: ");
	drake_platform_time_printf(stdout, duration);
	printf("\n");
*/
	}

	int res = drake_task_autoexit();
	//debug(res);
	return res;
}

int
drake_kill()
{
	//debug("update_pose");
	//drake_platform_time_get(killed[task->id - 1]);

	// Do nothing
	return 0;
}

int
drake_destroy()
{
	// Do nothing
	return 1;
}

