#include "util.h"

#include "vector_types.h"
#include <math.h>
#include <string.h>
#include <TooN/TooN.h>
#include <commons.h>

size_t
skip(size_t base, size_t ss, size_t aa)
{
	size_t kk = 4;
	/*
	float a = powf(k, (int)i * -1);
	float b = powf(k, (int)i) - 1;
	float c = a * b;
	float d = c * base;
	float e = d / (k - 1);

	return e;
	*/
	// n * (k^a - 1) * k^(1 - (s + 1)) / (k - 1);
	ptrdiff_t a = pow(kk, aa);
	ptrdiff_t b = a - 1;
	ptrdiff_t c = base * b;
	ptrdiff_t e = 1 - ss;
	float f = pow(kk, e);
	float g = c * f;
	ptrdiff_t h = kk - 1;
	ptrdiff_t i = g / h;
	return i;
	//return base * (pow(kk, aa - 1) - 1) * pow(kk, 1 - ss) / (kk - 1);
}

void
get_camera_matrix(float out[16], float k[4])
{
	memset(out, 0, sizeof(float) * 16);
	out[0] = k[0];
	out[2] = k[2];
	out[5] = k[1];
	out[6] = k[3];
	out[10] = 1;
	out[15] = 1;
}

void float4_scalar_div(float *res, float a[4], float b)
{
	res[0] = a[0] / b;
	res[1] = a[1] / b;
	res[2] = a[2] / b;
	res[3] = a[3] / b;
}

void get_inverse_camera_matrix(float *invK, const float *k) {
	// make_float4(1.0f / k.x, 0, -k.z / k.x, 0);
	invK[0] = 1.0f / k[0];
	invK[1] = 0;
	invK[2] = -k[2] / k[0];
	invK[3] = 0;

	// make_float4(0, 1.0f / k.y, -k.w / k.y, 0);
	invK[4] = 0;
	invK[5] = 1.0f / k[1];
	invK[6] = -k[3] / k[1];
	invK[7] = 0;

	// make_float4(0, 0, 1, 0);
	invK[8] = 0;
	invK[9] = 0;
	invK[10] = 1;
	invK[11] = 0;

	//make_float4(0, 0, 0, 1);
	invK[12] = 0;
	invK[13] = 0;
	invK[14] = 0;
	invK[15] = 1;
}

void vector3_add_vector3(float out[3], const float a[3], const float b[3])
{
	out[0] = a[0] + b[0];
	out[1] = a[1] + b[1];
	out[2] = a[2] + b[2];
}

void matrix4_multiply_matrix4(float out[16], float in[16], float m[16])
{
	TooN::wrapMatrix<4, 4>(&out[0]) = TooN::wrapMatrix<4, 4>(&in[0])
			* TooN::wrapMatrix<4, 4>(&m[0]);	
}

void inverse(float out[16], float in[16])
{
	static TooN::Matrix<4, 4, float> I = TooN::Identity;
	TooN::Matrix<4, 4, float> temp = TooN::wrapMatrix<4, 4>(&in[0]/*&A.data[0].x*/);
	//Matrix4 R;
	TooN::wrapMatrix<4, 4>(&out[0]/*&R.data[0].x*/) = TooN::gaussian_elimination(temp, I);
}

float get_volume_data(void *ptr, size_t x, size_t y, size_t z, unsigned int index)
{
	const Volume &vol = *(Volume*)ptr;
	//short2 point = vol.data[x + y * vol.size.x + z * vol.size.x * vol.size.y];
	if(index == 0)
	{
		//return point.x * 0.00003051944088f;
		return vol[make_uint3(x, y, z)].x;
	}
	else if(index == 1)
	{
		return vol[make_uint3(x, y, z)].y;
		//return point.y;
	}
	printf("[%s:%s:%d] Requested field #%u of a float2 structure.\n", __FILE__, __FUNCTION__, __LINE__, index + 1);
	abort();
} 

const void* get_volume_size(void *ptr)
{
	const Volume &vol = *(Volume*)ptr;
	return &vol.size;
} 

unsigned int read_uint2(const void* ptr, size_t index)
{
	const uint2 &val = *(uint2*)ptr;
	if(index == 0)
	{
		return val.x;
	}
	else if(index == 1)
	{
		return val.y;
	}
	printf("[%s:%s:%d] Requested field #%zu of a float2 structure.\n", __FILE__, __FUNCTION__, __LINE__, index + 1);
	abort();
}

unsigned int read_uint3(const void* ptr, size_t index)
{
	const uint3 &val = *(uint3*)ptr;
	if(index == 0)
	{
		return val.x;
	}
	else if(index == 1)
	{
		return val.y;
	}
	else if(index == 2)
	{
		return val.z;
	}
	printf("[%s:%s:%d] Requested field #%zu of a uint3 structure.\n", __FILE__, __FUNCTION__, __LINE__, index + 1);
	abort();
}

