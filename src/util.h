#include <stddef.h>
#include <drake/platform.h>

#ifndef UTIL_H
#define UTIL_H 1

// https://stackoverflow.com/questions/3585846/color-text-in-terminal-applications-in-unix
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#ifdef debug_color
#undef debug_color
#endif

#define debug_color(var, color) std::cout << (color ? color : "") << "[" << __FILE__ << ":" << __FUNCTION__ << ":" << __LINE__ << ":P" << drake_platform_core_id() << "][" << drake_task_name() << "] " << #var " = \"" << (var) << "\"" << KNRM << std::endl; fflush(NULL)

#define guard(var) debug_color(var, KGRN)
#define debug(var) debug_color(var, KNRM)

#ifdef __cplusplus
extern "C"
{
#endif 

size_t
skip(size_t base, size_t ss, size_t aa);

void
get_camera_matrix(float out[16], float k[4]);
void float4_scalar_div(float *res, float a[4], float b);
void get_inverse_camera_matrix(float *invK, const float *k);
void vector3_add_vector3(float out[3], const float a[3], const float b[3]);
void matrix4_multiply_matrix4(float out[16], float in[16], float m[16]);
void inverse(float out[16], float in[16]);
unsigned int read_uint2(const void* ptr, size_t index);
unsigned int read_uint3(const void* ptr, size_t index);
const void* get_volume_size(void *ptr);
float get_volume_data(void *ptr, size_t x, size_t y, size_t z, unsigned int index);

#ifdef __cplusplus
}
#endif

#endif
