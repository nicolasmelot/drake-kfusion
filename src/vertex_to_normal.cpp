/*
 Copyright 2015 Nicolas Melot

 This file is part of Drake-merge.

 Drake-merge is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Drake-merge is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Drake-merge. If not, see <http://www.gnu.org/licenses/>.

*/


#include <stdlib.h>
#include <math.h>
#include <float.h>

#include <drake.h>
#include <drake/link.h>
#include <string.h>

#include <drake-kfusion.h> 
#include "kernels.h"
#include "util.h"

drake_declare_input(input, float);
drake_declare_output(output, float);

static drake_kfusion_t *args;
static size_t i;
//static drake_time_t start, stop, duration;
//static link_tp input_link, output_link;

int
drake_init(void* arg)
{
	/*
	input_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->pred, "input")).value;
	output_link = pelib_map_read(const_string, link_tp)(pelib_map_find(const_string, link_tp)(task->succ, "output")).value;
	*/
	args = (drake_kfusion_t*)arg;
/*
	start = drake_platform_time_alloc();
	stop = drake_platform_time_alloc();
	duration = drake_platform_time_alloc();
*/
	i = 0;
	return 1;
}

int
drake_start()
{
	return 1;
}

#define PARALLEL_V2N 1
#if PARALLEL_V2N
struct v2n_args
{
	float *output, *input;
	uint2 compSize;
};

static int
parallel_v2n(void *aux)
{
	struct v2n_args *args = (struct v2n_args*)aux;
	vertex2normalKernel((float3*)args->output, (float3*)args->input, args->compSize, drake_task_core_id(), drake_task_width());

	return 1;
}
#endif

int
drake_run()
{
	// Fetch buffer addresses
	size_t input_size = 0;
	//float* input = (float*)pelib_cfifo_peekaddr(int)(input_link->buffer, 0, &input_size, NULL);
	float *input = drake_input_buffer(input)(0, &input_size, NULL);

	size_t output_size;
	//float *output = (float*)pelib_cfifo_writeaddr(int)(output_link->buffer, &output_size, NULL);
	float *output = drake_output_buffer(output)(&output_size, NULL);

	size_t inSize = 3 * args->compSize.x * args->compSize.y / pow(4, args->pyramid.size() - i - 1);
	size_t outSize = inSize;

#if DEBUG_INCONSISTENCIES
	//if(inSize > pelib_cfifo_capacity(int)(input_link->buffer) || outSize > pelib_cfifo_capacity(int)(output_link->buffer))
	if(inSize > drake_input_capacity(input) || outSize > drake_output_capacity(output))
	{
		printf("[%s:%s:%d] %s buffer cannot fit input or output frame (%zu > %zu)\n", __FILE__, __FUNCTION__, __LINE__, inSize > drake_input_capacity(input) ? "Input" : "Output", inSize > drake_input_capacity(input) ? inSize : outSize, inSize > drake_input_capacity(input) ? drake_input_capacity(input) : drake_output_capacity(output));
		abort();
	}
#endif

	if(input_size > inSize)
	{
		//pelib_cfifo_discard(int)(input_link->buffer, input_size);
		//debug(input_size);
		//debug(inSize);
		drake_input_discard(input)(input_size - inSize);
		input = drake_input_buffer(input)(0, &input_size, NULL);
	}

	//output = (float*)pelib_cfifo_writeaddr(int)(output_link->buffer, &output_size, &output_wrap);

#define DEBUG_GUARD 0
#if DEBUG_GUARD
debug("#### Vertex To Normal #####");
debug(inSize);
debug(outSize);
debug(input_size);
debug(output_size);
debug("########## End ############");
#endif
	if(input_size >= inSize && output_size >= outSize)
	{
		//debug(drake_task_instance());
		uint2 compSize = make_uint2(args->compSize.x / (int)pow(2, args->pyramid.size() - i - 1), args->compSize.y / (int)pow(2, args->pyramid.size() - i - 1));
	if(output_size > outSize)
	{
		//pelib_cfifo_fill(int)(output_link->buffer, output_size);
		//debug(output_size);
		//debug(outSize);
		drake_output_commit(output)(output_size - outSize);
		output = drake_output_buffer(output)(&output_size, NULL);
	}

//	drake_platform_time_get(start);
#if PARALLEL_V2N
#warning Parallel vertex2normal
	struct v2n_args pargs;
	pargs.output = output;
	pargs.input = input;
	pargs.compSize = compSize;
	drake_task_pool_run(parallel_v2n, &pargs);
#else
#warning Sequential vertex2normal
		vertex2normalKernel((float3*)output, (float3*)input, compSize, 0, 1);
#endif
//	drake_platform_time_get(stop);

		//pelib_cfifo_discard(int)(input_link->buffer, inSize);
		drake_input_discard(input)(inSize);
		//pelib_cfifo_fill(int)(output_link->buffer, outSize);
		drake_output_commit(output)(outSize);
		i = (i + 1) % args->pyramid.size();
//	drake_platform_time_subtract(duration, stop, start);
//	printf("vertex to normal: ");
//	drake_platform_time_printf(stdout, duration);
//	printf("\n");
	}

	inSize = 3 * args->compSize.x * args->compSize.y / pow(4, args->pyramid.size() - i - 1);
	outSize = inSize;

	// Either input channel is empty, either it is full of garbage because
	// of buffer wrapping performed by last task. In the latter case, we
	// should get rid of it. In the former case, we just discard 0 element.
	//return drake_task_depleted(task);
	int res = drake_task_autoexit();
	//debug(res);
	return res;
}

int
drake_kill()
{
	//debug("vertex_to_normal");
	//drake_platform_time_get(killed[task->id - 1]);

	// Do nothing
	return 0;
}

int
drake_destroy()
{
	// Do nothing
	return 1;
}

